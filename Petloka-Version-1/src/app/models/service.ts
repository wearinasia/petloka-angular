export interface Service {
  user_id: string;
  user_name: string;
  service_type: string;
  is_company: boolean;
  photo_url?: string | null;
  photo_list?: (string)[] | null;
  min_price: number;
  actual_min_price: number;
  location?: string | null;
  tagline: string;
  host_name?: string;
  total_rating: number;
}
