export class OrderList {
    booking_id: string;
    sequence: string;
    host: Host;
    customer: Customer;
    pet: Pet;
    product: Product;
    booking_address: BookingAddress;
    start_time?: string | null;
    end_time?: string | null;
    deduction: number;
    transport_fee: number;
    type: string;
    contact_name?: string | null;
    contact_number?: string | null;
    status: string;
    rating?: number | null;
    review?: string | null;
  length: number;
  }
  export interface Host {
    user_id: string;
    full_name: string;
    email: string;
    address: Address;
    phone_number: string;
    photo_url?: string | null;
    about_me: string;
    bank_info: BankInfo;
    birth_date?: null;
    verified: boolean;
    host: Host1;
  }
  export interface Address {
    street?: string | null;
    building?: string | null;
    city?: string | null;
    region?: string | null;
  }
  export interface BankInfo {
    bank_id?: null;
    bank_name: string;
    account_name?: string | null;
    account_number: string;
  }
  export interface Host1 {
    grooming: Grooming;
    hotel: Hotel;
  }
  export interface Grooming {
    service_type: string;
    host_name: string;
    is_company?: boolean | null;
    business_type?: null;
    phone_number?: string | null;
    enabled: boolean;
    feature?: null;
    count: number;
    coverage_area: CoverageArea;
    operationalDays: OperationalDays;
    operationalHours: OperationalHours;
    photo_urls?: (string)[] | null;
    tagline?: string | null;
    total_rating: number;
    reviews?: null;
    geolocation: Geolocation;
  }
  export interface CoverageArea {
    city?: string | null;
    region?: string | null;
  }
  export interface OperationalDays {
    weekDays?: (string | null)[] | null;
    weekEnd?: (string)[] | null;
  }
  export interface OperationalHours {
    weekDays: WeekDaysOrWeekEnd;
    weekEnd: WeekDaysOrWeekEnd;
  }
  export interface WeekDaysOrWeekEnd {
    end?: string | null;
    start?: string | null;
  }
  export interface Geolocation {
    type?: string | null;
    coordinates?: (string)[] | null;
  }
  export interface Hotel {
    service_type?: string | null;
    host_name?: string | null;
    is_company?: boolean | null;
    business_type?: null;
    phone_number?: string | null;
    enabled: boolean;
    feature?: null;
    count: number;
    coverage_area: CoverageArea;
    operationalDays: OperationalDays1;
    operationalHours: OperationalHours1;
    photo_urls?: (string)[] | null;
    tagline?: string | null;
    total_rating: number;
    reviews?: null;
    geolocation: Geolocation;
  }
  export interface OperationalDays1 {
    weekDays?: (string)[] | null;
    weekEnd?: null;
  }
  export interface OperationalHours1 {
    weekDays: WeekDaysOrWeekEnd;
    weekEnd: WeekEndOrWeekDays;
  }
  export interface WeekEndOrWeekDays {
    end?: null;
    start?: null;
  }
  export interface Customer {
    user_id: string;
    full_name?: string | null;
    email: string;
    address: Address;
    phone_number?: string | null;
    photo_url?: string | null;
    about_me?: string | null;
    bank_info: BankInfo1;
    birth_date?: null;
    verified: boolean;
  }
  export interface BankInfo1 {
    bank_id?: null;
    bank_name?: string | null;
    account_name?: string | null;
    account_number?: string | null;
  }
  export interface Pet {
    pet_id?: null;
    pet_name?: null;
    pet_race?: null;
    pet_size?: null;
    user_id?: null;
  }
  export interface Product {
    product_id: string;
    about?: string | null;
    including?: (string | null)[] | null;
    rules?: string | null;
    note?: string | null;
    price: number;
    deducted_price: number;
    total_rating: number;
    region?: null;
    service_type: string;
    product_name: string;
    pet_size?: null;
    pet_type?: null;
    location: GeolocationOrLocation;
  }
  export interface GeolocationOrLocation {
    type?: null;
    coordinates?: null;
  }
  export interface BookingAddress {
    street?: string | null;
    building?: string | null;
    city?: string | null;
    region?: null;
  }
  