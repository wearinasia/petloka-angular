export class Profile {
  user_id: string;
  full_name: string;
  email: string;
  address: Address;
  phone_number: string;
  photo_url: string;
  about_me: string;
  bank_info: BankInfo;
  birth_date?: null;
  verified: boolean;
  host: Host;
}
export class Address {
  street: string;
  building: string;
  city: string;
  region: string;
}
export class BankInfo {
  bank_id?: null;
  bank_name: string;
  account_name: string;
  account_number: string;
}
export class Host {
  grooming: GroomingOrHotel;
  hotel: GroomingOrHotel;
}
export class GroomingOrHotel {
  service_type: string;
  host_name: string;
  is_company: boolean;
  business_type?: null;
  phone_number?: null;
  enabled: boolean;
  feature?: null;
  count: number;
  coverage_area: CoverageArea;
  operationalDays: OperationalDays;
  operationalHours: OperationalHours;
  photo_urls?: (string)[] | null;
  tagline: string;
  total_rating: number;
  reviews?: null;
  geolocation: Geolocation;
}
export class CoverageArea {
  city: string;
  region?: null;
}
export class OperationalDays {
  weekDays?: (string)[] | null;
  weekEnd?: null;
}
export class OperationalHours {
  weekDays: WeekDays;
  weekEnd: WeekEnd;
}
export class WeekDays {
  end: string;
  start: string;
}
export class WeekEnd {
  end?: null;
  start?: null;
}
export class Geolocation {
  type: string;
  coordinates?: (number)[] | null;
  radius: number;
}
