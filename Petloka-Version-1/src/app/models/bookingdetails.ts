export class BookingDetails {
  booking_id: string;
  sequence: string;
  host: Host;
  customer: Customer;
  pet: Pet;
  product: Product;
  booking_address: BookingAddress;
  start_time: string;
  end_time?: null;
  deduction: number;
  transport_fee: number;
  type: string;
  contact_name: string;
  contact_number: string;
  status: string;
  rating: number;
  review: string;
}
export class Host {
  user_id: string;
  full_name: string;
  email: string;
  address: Address;
  phone_number: string;
  photo_url: string;
  about_me: string;
  bank_info: BankInfo;
  birth_date?: null;
  verified: boolean;
  last_active: string;
  host: Host1;
}
export class Address {
  street: string;
  building: string;
  city: string;
  region: string;
}
export class BankInfo {
  bank_id?: null;
  bank_name: string;
  account_name: string;
  account_number: string;
}
export class Host1 {
  grooming: GroomingOrHotel;
  hotel: GroomingOrHotel;
}
export class GroomingOrHotel {
  service_type: string;
  host_name: string;
  is_company: boolean;
  business_type?: null;
  phone_number: string;
  enabled: boolean;
  feature?: null;
  count: number;
  coverage_area: CoverageArea;
  operationalDays: OperationalDays;
  operationalHours: OperationalHours;
  photo_urls?: (string)[] | null;
  tagline: string;
  total_rating: number;
  reviews?: null;
  geolocation: Geolocation;
}
export class CoverageArea {
  city: string;
  region?: null;
}
export class OperationalDays {
  weekDays?: (string)[] | null;
  weekEnd?: null;
}
export class OperationalHours {
  weekDays: WeekDays;
  weekEnd: WeekEnd;
}
export class WeekDays {
  end: string;
  start: string;
}
export class WeekEnd {
  end?: null;
  start?: null;
}
export class Geolocation {
  type: string;
  coordinates?: (string)[] | null;
}
export class Customer {
  user_id: string;
  full_name: string;
  email: string;
  address: Address;
  phone_number: string;
  photo_url: string;
  about_me: string;
  bank_info: BankInfo;
  birth_date?: null;
  verified: boolean;
  last_active: string;
}
export class Pet {
  pet_id?: null;
  pet_name?: null;
  pet_race?: null;
  pet_size?: null;
  user_id?: null;
}
export class Product {
  product_id: string;
  about: string;
  including?: (string)[] | null;
  rules?: null;
  note: string;
  price: number;
  deducted_price: number;
  total_rating: number;
  region?: null;
  service_type: string;
  product_name: string;
  pet_size?: null;
  pet_type?: null;
  location: Location;
}
export class Location {
  type?: null;
  coordinates?: null;
}
export class BookingAddress {
  street: string;
  building?: null;
  city?: null;
  region?: null;
}
