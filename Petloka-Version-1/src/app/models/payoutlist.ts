export class PayoutList {
    payout_id: string;
    booking: Booking;
    user: User;
    status: string;
    product: Product;
  }
  export class Booking {
    booking_id: string;
    sequence: string;
    user_id: string;
    host: string;
    requester: string;
    pet_id?: null;
    product_id: string;
    product_name: string;
    booking_address: BookingAddress;
    start_time: string;
    end_time?: string | null;
    deduction: number;
    transport_fee: number;
    type: string;
    contact_name: string;
    contact_number: string;
    status: string;
    rating?: number | null;
    review?: string | null;
  }
  export class BookingAddress {
    street: string;
    building?: string | null;
    city?: string | null;
    region?: null;
  }
  export class User {
    user_id: string;
    full_name?: null;
    email: string;
    address: Address;
    phone_number: string;
    photo_url?: null;
    about_me?: null;
    bank_info: BankInfo;
    birth_date?: null;
    verified: boolean;
    last_active?: null;
  }
  export class Address {
    street?: null;
    building?: null;
    city?: null;
    region?: null;
  }
  export class BankInfo {
    bank_id?: null;
    bank_name?: null;
    account_name?: null;
    account_number?: null;
  }
  export class Product {
    product_id: string;
    about: string;
    including?: (string)[] | null;
    rules?: string | null;
    note: string;
    price: number;
    service_type: string;
    product_name: string;
  }
  