export class Product {
  product_id: string;
  about: string;
  including?: (string)[] | null;
  rules: string;
  note: string;
  price: number;
  deducted_price: number;
  total_rating: number;
  region?: null;
  service_type: string;
  product_name: string;
  pet_size?: null;
  pet_type?: null;
  user_id: string;
  location: Location;
}

export class Location {
  type: string;
  coordinates?: (string)[] | null;
}
