export class MyBooking {
    booking_id: string;
    sequence: string;
    host: Host;
    customer: Customer;
    pet: Pet;
    product: Product;
    booking_address: AddressOrBookingAddress;
    start_time?: string | null;
    end_time?: string | null;
    deduction: number;
    type: string;
    contact_name?: string | null;
    contact_number?: string | null;
    status: string;
    rating?: null;
    review?: null;
  }

  export class Host {
    user_id: string;
    full_name: string;
    email: string;
    address: AddressOrBookingAddress;
    phone_number: string;
    photo_url: string;
    about_me: string;
    bank_info: BankInfo;
    birth_date?: null;
    verified: boolean;
  }

  export class AddressOrBookingAddress {
    street?: string | null;
    building?: string | null;
    city?: string | null;
    region?: string | null;
  }

  export class BankInfo {
    bank_id?: null;
    bank_name: string;
    account_name: string;
    account_number: string;
  }

  export class Customer {
    user_id: string;
    full_name: string;
    email: string;
    address: AddressOrBookingAddress1;
    phone_number: string;
    photo_url?: null;
    about_me: string;
    bank_info: BankInfo;
    birth_date?: null;
    verified: boolean;
  }

  export class AddressOrBookingAddress1 {
    street?: null;
    building?: null;
    city?: null;
    region?: null;
  }

  export class Pet {
    pet_id?: null;
    pet_name?: null;
    pet_race?: null;
    pet_size?: null;
    user_id?: null;
  }

  export class Product {
    product_id: string;
    about: string;
    including?: (string)[] | null;
    rules?: null;
    note: string;
    price: number;
    deducted_price: number;
    total_rating: number;
    region?: null;
    service_type: string;
    product_name: string;
    pet_size?: null;
    pet_type?: null;
  }
  