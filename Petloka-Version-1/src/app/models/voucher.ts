export class Voucher {
    applied_assets?: (string)[] | null;
    applied_services?: (string)[] | null;
    date_end?: string | null;
    date_start?: string | null;
    description?: string | null;
    enabled: boolean;
    id: string;
    promo_type?: string | null;
    rule_name?: string | null;
    rule_type?: string | null;
    sequence: number;
    value: number;
  }
  