export class Auth{
    
    id: number;
    idToken: string;
    regToken: string;
    expiry: string;
    localId: string;
    loggedIn: boolean;
    
}