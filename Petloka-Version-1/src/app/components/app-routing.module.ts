import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

//admin
import { OrderListComponent } from './AdminModule/sales/order-list/order-list.component';
import { OrderDetailsComponent } from './AdminModule/sales/order-details/order-details.component';
import { PayoutListComponent } from './AdminModule/payout/payout-list/payout-list.component';
import { PayoutDetailsComponent } from './AdminModule/payout/payout-details/payout-details.component';
import { CreateVoucherComponent } from './AdminModule/create-voucher/create-voucher.component';
import { VoucherListComponent } from './AdminModule/create-voucher/voucher-list/voucher-list.component';

//homepage
import { HomepageComponent } from './Homepage/homepage.component';

//EditProfilebyId
import { EditProfileByIdComponent } from './UserModule/edit-profile-by-id/edit-profile-by-id.component';

//auth
import { LoginComponent } from './AuthModule/login/login.component';
import { RegisterComponent } from './AuthModule/register/register.component';
import { ForgotpasswordComponent } from './AuthModule/forgotpassword/forgotpassword.component';
import { AuthGuard } from './AuthModule/auth.guard';

//catalog
import { ServiceComponent } from '../components/CatalogModule/service/service.component';
import { ProductComponent } from '../components/CatalogModule/product/product.component';

//customer account
import { CustomerAccountComponent } from '../components/UserModule/customer-account/customer-account.component';
import { MyBookingComponent } from './UserModule/customer-account/mybooking/mybooking.component';
import { MyBookingDetailsComponent } from './UserModule/customer-account/mybooking-details/mybooking-details.component';

//business account
import { BusinessAccountComponent } from '../components/UserModule/business-account/business-account.component';
import { BusinessOrderComponent } from '../components/UserModule/business-account/business-order/business-order.component';
import { BusinessManageAccountComponent } from '../components/UserModule/business-account/business-manage-account/business-manage-account.component';
import { BusinessBankAccountComponent } from './UserModule/business-account/business-bank-account/business-bank-account.component';
import { BusinessAccountProductComponent } from './UserModule/business-account/business-manage-account/business-account-product/business-account-product.component';
import { MobileBusinessManageAccountComponent } from './UserModule/business-account/mobile-business-manage-account/mobile-business-manage-account.component';

//booking
import { CreateBookingComponent } from '../components/BookingModule/create-booking/create-booking.component';
import { BookingDetailsComponent } from '../components/BookingModule/create-booking/booking-details/booking-details.component';
import { FormGuard } from './BookingModule/form.guard';
import { CustomerEditProfileComponent } from './UserModule/customer-account/customer-edit-profile/customer-edit-profile.component';
import { BuinessEditProfileComponent } from './UserModule/business-account/buiness-edit-profile/buiness-edit-profile.component';
import { EditContactProfileComponent } from './BookingModule/create-booking/edit-contact-profile/edit-contact-profile.component';
import { VoucherComponent } from './BookingModule/create-booking/voucher/voucher.component';

//CMS
import { PrivacyViewComponent } from './CMSModule/privacy/privacy-view/privacy-view.component';
import { PrivacyListComponent } from './CMSModule/privacy/privacy-list/privacy-list.component';
import { HelpViewComponent } from './CMSModule/help/help-view/help-view.component';
import { RefundPolicyViewComponent } from './CMSModule/refund-policy/refund-policy-view/refund-policy-view.component';

const routes: Routes = [
 // { path: '', redirectTo: '/homepage', pathMatch: 'full' },
  
  //home
  { path: '', component: HomepageComponent },
  //EditProfileById
  { path: 'editprofile/:password', component: EditProfileByIdComponent },
  //auth
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  // { path: 'forgot', component: ForgotpasswordComponent },
  //catalog
  { path: 'service/type/:type', component: ServiceComponent},
  { path: 'service/type', component: ServiceComponent},
  { path: 'product/id/:uId/service/type/:type', component: ProductComponent },
  //customer
  { path: 'account/customer', component: CustomerAccountComponent },
  { path: 'account/customer/profile', component: CustomerEditProfileComponent, canActivate:[AuthGuard] },
  { path: 'account/customer/booking', component: MyBookingComponent, canActivate:[AuthGuard] },
  { path: 'account/customer/booking/details/id/:bId', component: MyBookingDetailsComponent},
  //business
  { path: 'account/business', component: BusinessAccountComponent},
  { path: 'account/business/orders', component: BusinessOrderComponent , canActivate:[AuthGuard] },
  { path: 'account/business/orders/details/id/:bId', component: MyBookingDetailsComponent },
  { path: 'account/business/edit-service', component: BuinessEditProfileComponent, canActivate:[AuthGuard]},
  { path: 'account/business/manage-service', component: BusinessManageAccountComponent, canActivate:[AuthGuard]},
  { path: 'account/business/manage-service/:uId', component: MobileBusinessManageAccountComponent},
  { path: 'account/business/bank-account', component: BusinessBankAccountComponent, canActivate:[AuthGuard]},
  //booking
  { path: 'createbooking', component: CreateBookingComponent, canActivate:[FormGuard] },
  { path: 'bookingdetails', component: BookingDetailsComponent, canActivate:[FormGuard] },
  { path: 'edit-contact', component: EditContactProfileComponent },
  { path: 'add-voucher', component: VoucherComponent },
  //admin
  { path: 'admin/sales/order-list', component: OrderListComponent },
  { path: 'admin/sales/order-details/id/:bId', component: OrderDetailsComponent },
  { path: 'admin/payout/payout-list', component: PayoutListComponent },
  { path: 'admin/payout/payout-details/id/:bId', component: PayoutDetailsComponent },
  { path: 'admin/promo/create-voucher', component: CreateVoucherComponent },
  { path: 'admin/promo/voucher-list', component: VoucherListComponent },
  //CMS
  { path: 'privacy', component: PrivacyViewComponent },
  { path: 'privacy/:No', component: PrivacyListComponent },
  { path: 'refund-policy', component: RefundPolicyViewComponent },
  { path: 'help', component: HelpViewComponent },
  //not found
  { path: '**', component: HomepageComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}