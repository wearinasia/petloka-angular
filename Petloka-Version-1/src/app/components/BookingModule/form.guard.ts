import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormGuard implements CanActivate{
  constructor(private router: Router, private route: ActivatedRoute) {
  }

  step: Number;
  pId: string;

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      this.route.queryParams.subscribe(params => {
        this.step = Number(params.step);
      });
      const cartSession = JSON.parse(localStorage.getItem("cartSession"));

      if ( localStorage.getItem('bookingSession') != null){
         return true
       }  else if( cartSession.pId == null){
        alert("Select Product First");
        this.router.navigate([""]);
        return false;
        }
        else if( cartSession != null){
          return true;
        }
       else  {
         alert("Please fill the form first");
         this.router.navigate(["/createbooking"], {queryParams: {step: 1}});
         return false
       }
 
  }
}
