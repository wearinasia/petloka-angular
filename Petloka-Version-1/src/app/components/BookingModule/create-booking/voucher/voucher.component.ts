import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { VoucherService } from '../../../../services/voucher.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../../components/shared/dialog/dialog.component';
import { Location } from '@angular/common';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.css']
})
export class VoucherComponent implements OnInit {

  voucher = new FormControl('');
  faTimes = faTimes;
  pPrice: string;
  pId: string;
  cartSession;
  bookingSession;
  dialogRef: MatDialogRef<DialogComponent>;

  constructor(private voucherService: VoucherService, private dialog: MatDialog, private location: Location) {}

  ngOnInit() {
    this.pPrice = localStorage.getItem('itemPrice');
    this.cartSession = JSON.parse(localStorage.getItem('cartSession'));
    this.bookingSession = JSON.parse(localStorage.getItem('bookingSession'));
    if(this.bookingSession.voucherCode != null){
      this.voucher.setValue(this.bookingSession.voucherCode);
    }
    this.pId = this.cartSession.pId;
  }

  clear(){
    this.voucher.setValue('');
    const bookingSession = {
      booking_address:{
        building:   this.bookingSession.booking_address.building,
        street:     this.bookingSession.booking_address.street
      },
      contact_name:   this.bookingSession.contact_name,
      contact_number: this.bookingSession.contact_number,
      deduction:      0,
      end_time:       this.bookingSession.end_time,
      start_time:     this.bookingSession.start_time,
      transport_fee:  this.bookingSession.transport_fee,
      type:           this.bookingSession.type,
      voucherCode:    this.voucher.value
    }
    localStorage.setItem('bookingSession', JSON.stringify(bookingSession));
    this.dialogRef = this.dialog.open(DialogComponent, {
      data: {
        message: "No Voucher",
        button: "back"
      }
    });
  }

  checkVoucher(){
    if(this.voucher.value == this.bookingSession.voucherCode){
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Voucher has been applied",
          button: "back"
        }
      });
    }else{
    this.voucherService.voucherValidation(this.voucher.value,this.pId)
    .subscribe(valid => {
      if(valid.status == "success"){
        this.voucherService.getVoucherAmount(this.pPrice, this.voucher.value)
        .subscribe(amount => {
          localStorage.removeItem('itemPrice');
          const bookingSession = {
            booking_address:{
              building:   this.bookingSession.booking_address.building,
              street:     this.bookingSession.booking_address.street
            },
            contact_name:   this.bookingSession.contact_name,
            contact_number: this.bookingSession.contact_number,
            deduction:      amount,
            end_time:       this.bookingSession.end_time,
            start_time:     this.bookingSession.start_time,
            transport_fee:  this.bookingSession.transport_fee,
            type:           this.bookingSession.type,
            voucher_id:     this.voucher.value
          }
          localStorage.setItem('bookingSession', JSON.stringify(bookingSession));
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: valid.message,
              button: "back"
            }
          });
        });
      }else{
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: valid.message,
            button: "back"
          }
        });
      }
     })
    }
  }

}
