import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { BookingService } from '../../../../services/booking.service';
import { formatDate } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { ProductService } from '../../../../services/product.service';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-booking-details',
  templateUrl: './booking-details.component.html',
  styleUrls: ['./booking-details.component.css']
})
export class BookingDetailsComponent implements OnInit {

  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  private cartSession = JSON.parse(localStorage.getItem('cartSession'));
  faChevronRight = faChevronRight;
  productDetails;
  total: number;
  street: string;
  building: string;
  startDate: string;
  endDate: string;
  startTime: string;
  endTime: string;
  type: string;
  contactName: string;
  contactNumber: string;
  bookingId: string;
  date: Date;

  pId: string;
  pName: string;
  pPrice: number;
  discountPrice: number;
  transportationFee: number;
  selectedBank = 'BCA';

  constructor(private router: Router, private route: ActivatedRoute, private bookingService: BookingService, private prodService: ProductService, private pageTitle: Title) { }

  ngOnInit() {
      this.pageTitle.setTitle('Booking Details');
      const details = JSON.parse(localStorage.getItem("bookingSession"));
      this.discountPrice = +details.deduction;
      this.street = details.booking_address.street;
      this.building = details.booking_address.building;
      this.startDate = formatDate(details.start_time, 'shortDate', 'id', '+0700');
      this.startTime = formatDate(details.start_time, 'shortTime', 'id', '+0700');

      if(details.end_time == ''){
      this.date = new Date(details.start_time);
      this.endDate = formatDate(this.date.setDate(this.date.getDate() + 1), 'shortDate', 'id', '+0700') + ' | Limit for groomer';
      } else {
        this.endDate = formatDate(details.end_time, 'shortDate', 'id', '+0700')
      }

      this.type = details.type;
      this.contactName = details.contact_name;
      this.contactNumber = details.contact_number;
      this.transportationFee = details.transport_fee;

      this.pId = this.cartSession.pId;

      this.prodService.getProductDetails(this.pId)
      .subscribe(productDetails => {
        this.pName = productDetails.product_name;
        this.productDetails = productDetails;
        if(this.discountPrice != null){
           this.pPrice = +productDetails.price;
           this.total = (this.pPrice - this.discountPrice) + this.transportationFee;
           if(this.discountPrice > this.pPrice){
             this.discountPrice = this.pPrice;
             this.total = (this.pPrice - this.discountPrice) + this.transportationFee;
           }
        } else {
          this.pPrice = +productDetails.price;
          this.total = this.pPrice + this.transportationFee;
        }
      })
  }

  toVoucher(){
    this.router.navigateByUrl('/add-voucher');
    localStorage.setItem('itemPrice', this.pPrice.toString());
  }

  submit(){
      const details = JSON.parse(localStorage.getItem("bookingSession"));
      this.bookingService.createBooking(details, this.pId).subscribe(
        booking => {
          if(booking.booking_id == null){
            window.alert("Session Invalid");
            this.router.navigateByUrl('');
          } else {
            this.bookingId = JSON.stringify(booking.booking_id);
            localStorage.removeItem('cartSession');
            localStorage.removeItem('bookingSession');
            localStorage.removeItem('destination');
            localStorage.removeItem('position');
            localStorage.setItem('bookingId', JSON.parse(this.bookingId));
            this.router.navigateByUrl('account/customer/booking');
          }
        });
    
  }

  submitMidTrans(){
    const details = JSON.parse(localStorage.getItem("bookingSession"));
    if(this.selectedBank != undefined){
    this.bookingService.createBooking(details, this.pId).subscribe(
      booking => {
        if(booking.booking_id == null){
          window.alert("Session Invalid");
          this.router.navigateByUrl('');
        } else {
          this.bookingId = booking.booking_id;
          localStorage.removeItem('cartSession');
          localStorage.removeItem('bookingSession');
          // localStorage.setItem('bookingId', JSON.parse(this.bookingId));
          console.log({"bank_transfer": {
            "bank" : this.selectedBank
          },
          "customer_details": {
            "first_name": this.contactName,
            "phone": this.contactNumber
          },
          "item_details": {
            "id": this.pId,
            "price": this.pPrice,
            "quantity": 1,
            "name": this.pName
          },
          "payment_type": "bank_transfer",
          "transaction_details":{
            "order_id": this.bookingId,
            "gross_amount": this.total
          }})
          this.bookingService.bookingPayment({
            "bank_transfer": {
              "bank" : this.selectedBank
            },
            "customer_details": {
              "first_name": this.contactName,
              "phone": this.contactNumber
            },
            "item_details": {
              "id": this.pId,
              "price": this.pPrice,
              "quantity": 1,
              "name": this.pName
            },
            "payment_type": "bank_transfer",
            "transaction_details":{
              "order_id": this.bookingId,
              "gross_amount": this.total
            }
          }).subscribe(response =>{
            console.log(response);
            this.router.navigate(['/account/customer/booking/details/id/'+this.bookingId])
          })
          // delay(this.router.navigateByUrl('account/customer/booking'), 20);
        }
      });
    }
  }

}
