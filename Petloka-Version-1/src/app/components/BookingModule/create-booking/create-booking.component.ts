import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Router } from '@angular/router';
import { ProductService } from '../../../services/product.service';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ServiceService } from 'src/app/services/service.service';
import { AuthService } from '../../../services/auth.service';
import { BookingService } from '../../../services/booking.service';
import { ProfileService } from '../../../services/profile.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../shared/dialog/dialog.component';
import { Profile } from '../../../models/profile';

export const CUSTOM_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
}


@Component({
  selector: 'app-create-booking',
  templateUrl: './create-booking.component.html',
  styleUrls: ['./create-booking.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'id'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_FORMATS},
  ]
})
export class CreateBookingComponent implements OnInit {

  bookingSession: any;
  dialogRef : MatDialogRef<DialogComponent>;
  step: number;
  productDetails;
  serviceType: string;
  matcher: string;
  bookingForm: FormGroup;
  private cartSession = JSON.parse(localStorage.getItem("cartSession"));
  public origin: string;
  loggedIn: boolean;
  pId: string;
  transportationFee: number;
  profileDetails: Profile;

  latitude: number = -6.175387099999986;
  longitude: number = 106.82714748255614;
  radius: number = 5000;
  zoom: number;
  address: string;
  private geoCoder;
  @ViewChild('search', {static: false})
  public searchElementRef: ElementRef;

  constructor(private profService: ProfileService, private ngZone: NgZone, private dialog: MatDialog, private bookingService: BookingService, private authService: AuthService, private router: Router, private prodService: ProductService, private pageTitle: Title, private servService: ServiceService) {
    this.initializeForm();
    if(localStorage.getItem("bookingSession") == null){
      this.bookingForm.patchValue(this.bookingForm);
    }else {
      const latestForm = JSON.parse(localStorage.getItem("bookingSession"));
      this.bookingForm.setValue(latestForm);
    }
  }

  ngOnInit() {
    this.loggedIn = this.authService.getLoggedIn();
    this.pageTitle.setTitle('Create Booking');
    if(this.loggedIn == false){
      this.router.navigate(['/login'], {queryParams: {lastUrl: 'createbooking'}})
    } else {
      this.pageTitle.setTitle('Create Booking');
      this.pId = this.cartSession.pId;
      this.serviceType = this.servService.getServiceType();

      this.prodService.getProductDetails(this.pId)
      .subscribe(productDetails => {
        if(productDetails.user_id == this.authService.getCustomerId()){
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: "Can't book your own Services",
              button: 'back'
            }
          });
        }else {
        localStorage.setItem('destination', JSON.stringify({"longitude": +productDetails.location.coordinates[0], "latitude": +productDetails.location.coordinates[1]}));
        this.productDetails = productDetails;
            this.profService.getProfileDetails()
            .subscribe(profileDetails => {
              this.bookingForm.patchValue({
                contact_name: profileDetails.full_name,
                contact_number: profileDetails.phone_number,
                booking_address: {
                  street: profileDetails.address.street,
                  building: profileDetails.address.building
                }
              });
              this.profileDetails = profileDetails;
            });
          }
      })
    }
  }

  submit(){
    if(this.bookingForm.get('type').value == 'House Call'){
        const origin = JSON.parse(localStorage.getItem('position'));
        const destination = JSON.parse(localStorage.getItem('destination'));
        this.bookingService.getTransportationFee(origin, destination)
        .subscribe(transportationFee => {
        this.transportationFee = transportationFee.price;
        this.bookingForm.get('transport_fee').setValue(this.transportationFee);
        this.bookingSession = this.bookingForm.value;
        localStorage.setItem('bookingSession', JSON.stringify(this.bookingSession));
        this.router.navigate(['/bookingdetails'], {queryParams: {step: 2}});
          })
        } 
    
     else {
        this.bookingForm.get('transport_fee').setValue(0);
        this.bookingSession = this.bookingForm.value;
        localStorage.setItem('bookingSession', JSON.stringify(this.bookingSession));
        this.router.navigate(['/bookingdetails'], {queryParams: {step: 2}});
    }
  }

  initializeForm(){
    this.bookingForm =  new FormGroup({
      booking_address:  new FormGroup({
        street:         new FormControl('',  [Validators.required]),
        building:       new FormControl(''),
      }),
      start_time:      new FormControl('', [Validators.required]),
      end_time:        new FormControl (''),
      type:            new FormControl('', [Validators.required]),
      transport_fee:   new FormControl(''),
      deduction:       new FormControl(''),
      contact_name:    new FormControl ('', [Validators.required]),
      contact_number:  new FormControl('', [Validators.required, Validators.pattern(/^-?(0|[0-9]\d*)?$/)]),
      voucherCode:     new FormControl('')
    });
  }
}
