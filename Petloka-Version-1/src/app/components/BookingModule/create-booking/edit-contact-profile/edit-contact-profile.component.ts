import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { ProfileService } from '../../../../services/profile.service';
import { Profile } from '../../../../models/profile';
import { MapsAPILoader, MouseEvent } from '@agm/core';

@Component({
  selector: 'app-edit-contact-profile',
  templateUrl: './edit-contact-profile.component.html',
  styleUrls: ['./edit-contact-profile.component.css']
})
export class EditContactProfileComponent implements OnInit {

  contactDetailsForm: FormGroup;
  profileDetails: Profile;

  latitude: number = -6.175387099999986;
  longitude: number = 106.82714748255614;
  radius: number = 5000;
  zoom: number;
  address: string;
  private geoCoder;
  @ViewChild('search', {static: false})
  public searchElementRef: ElementRef;
  deviceInfo = null;

  constructor(private ngZone: NgZone, private mapsAPILoader: MapsAPILoader, private profService: ProfileService, private router: Router) { 
    this.initializeForm();
  }

  ngOnInit() {
    this.profService.getProfileDetails()
    .subscribe(profileDetails => {
      this.profileDetails = profileDetails;
      this.contactDetailsForm.patchValue(this.profileDetails);

      this.mapsAPILoader.load().then(() => {
        this.setCurrentLocation();
        this.geoCoder = new google.maps.Geocoder;
   
        let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ["address"]
        });
        autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
            let place: google.maps.places.PlaceResult = autocomplete.getPlace();
   
            if (place.geometry === undefined || place.geometry === null) {
              return;
            }
            
            
            this.latitude = place.geometry.location.lat();
            this.longitude = place.geometry.location.lng();
            this.getAddress(this.latitude, this.longitude);
            this.zoom = 12;
          });
        });
      });
    })
  }

  submit(){
    this.profService.editProfile(this.contactDetailsForm.value)
    .subscribe(response => {
      this.router.navigateByUrl('/createbooking', {queryParams: {steps: 1}})
    })
  }

  initializeForm(){
    this.contactDetailsForm =  new FormGroup({
      full_name:    new FormControl('', [Validators.required]),
      phone_number: new FormControl('', [Validators.required, Validators.pattern(/^-?(0|[0-9]\d*)?$/)]),
      address:      new FormGroup({
        street:     new FormControl('', [Validators.required]),
        building:   new FormControl('', [Validators.required])
      })
    });
  }

  setCurrentLocation(){
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = +position.coords.latitude;
        this.longitude = +position.coords.longitude;
        this.getAddress(this.latitude, this.longitude);
      });
    }
  }

  markerDragEnd(event: MouseEvent) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
          localStorage.setItem('position', JSON.stringify({"longitude": longitude, "latitude": latitude}));
          // this.contactDetailsForm.get('address').get('street').setValue(this.address);
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }

}
