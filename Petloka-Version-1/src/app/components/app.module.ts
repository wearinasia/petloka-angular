import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { ConnectionServiceModule } from 'ng-connection-service';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import localeid from '@angular/common/locales/id';
registerLocaleData(localeid);

import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';

import { AppComponent } from './app.component';
import { CardServiceListComponent } from './shared/card-service-list/card-service-list.component';
import { CardServiceHotelComponent } from './shared/card-service-profile/card-service-profile.component';
import { CardBookingListComponent } from './shared/card-booking-list/card-booking-list.component';
import { CardProductListComponent } from './shared/card-product-list/card-product-list.component';

import { AppRoutingModule }     from './app-routing.module';
import { LoginComponent } from './AuthModule/login/login.component';
import { RegisterComponent } from './AuthModule/register/register.component';
import { ForgotpasswordComponent } from './AuthModule/forgotpassword/forgotpassword.component';
import { HttpErrorInterceptor } from '../services/http-error.interceptor';
import { AuthGuard } from './AuthModule/auth.guard';
import { DeferLoadModule } from '@trademe/ng-defer-load';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatChipsModule } from '@angular/material/chips';
import { MatProgressSpinnerModule, MatProgressSpinner } from '@angular/material/progress-spinner';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { CalendarModule } from '@syncfusion/ej2-angular-calendars';
import { TimePickerModule } from '@syncfusion/ej2-angular-calendars';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { DeviceDetectorModule } from 'ngx-device-detector';
import { AgmCoreModule } from '@agm/core';

import { ProductComponent } from './CatalogModule/product/product.component';
import { ServiceComponent } from './CatalogModule/service/service.component';
import { CustomerAccountComponent } from './UserModule/customer-account/customer-account.component';
import { MyBookingComponent } from './UserModule/customer-account/mybooking/mybooking.component';
//import { ProfileComponent } from './UserModule/customer-account/profile/profile.component';
//import { SidebarComponent } from './UserModule/customer-account/sidebar/sidebar.component';
import { CustomerSidebarComponent } from './UserModule/customer-account/customer-sidebar/customer-sidebar.component';
import { CreateBookingComponent } from './BookingModule/create-booking/create-booking.component';
import { BookingDetailsComponent } from './BookingModule/create-booking/booking-details/booking-details.component';
import { registerLocaleData } from '@angular/common';
import { BusinessAccountComponent } from './UserModule/business-account/business-account.component';
import { BusinessSidebarComponent } from './UserModule/business-account/business-sidebar/business-sidebar.component';
import { MyBookingDetailsComponent } from './UserModule/customer-account/mybooking-details/mybooking-details.component';
import { BusinessOrderComponent } from './UserModule/business-account/business-order/business-order.component';
import { MainHeaderComponent } from './Global/Header/main-header/main-header.component';
import { MainFooterComponent } from './Global/Footer/main-footer/main-footer.component';
import { BookingFooterComponent } from './Global/Footer/booking-footer/booking-footer.component';
import { BookingHeaderComponent } from './Global/Header/booking-header/booking-header.component';
import { HomepageComponent } from './Homepage/homepage.component';
import { BusinessManageAccountComponent } from './UserModule/business-account/business-manage-account/business-manage-account.component';
import { BusinessBankAccountComponent } from './UserModule/business-account/business-bank-account/business-bank-account.component';
import { BusinessAccountProductComponent } from './UserModule/business-account/business-manage-account/business-account-product/business-account-product.component';
import { BusinessAccountScheduleComponent } from './UserModule/business-account/business-manage-account/business-account-schedule/business-account-schedule.component';
import { BusinessAccountPhotosComponent } from './UserModule/business-account/business-manage-account/business-account-photos/business-account-photos.component';
import { HomepageSearchComponent } from './Homepage/homepage-search/homepage-search.component';
import { MainUspComponent } from './Global/Footer/main-usp/main-usp.component';
import { DownloadCTAComponent } from './Global/Footer/download-cta/download-cta.component';
import { EditProfileByIdComponent } from './UserModule/edit-profile-by-id/edit-profile-by-id.component';
import { MyscheduleComponent } from './UserModule/customer-account/myschedule/myschedule.component';
import { CustomerEditProfileComponent } from './UserModule/customer-account/customer-edit-profile/customer-edit-profile.component';
import { BuinessEditProfileComponent } from './UserModule/business-account/buiness-edit-profile/buiness-edit-profile.component';
import { DialogComponent } from './shared/dialog/dialog.component';
import { MobileBusinessManageAccountComponent } from './UserModule/business-account/mobile-business-manage-account/mobile-business-manage-account.component';
import { OrderListComponent } from './AdminModule/sales/order-list/order-list.component';
import { OrderDetailsComponent } from './AdminModule/sales/order-details/order-details.component';
import { TrackerComponent } from './shared/tracker/tracker.component';
import { BlockServiceListComponent } from './shared/block/block-service-list/block-service-list.component';
import { PayoutListComponent } from './AdminModule/payout/payout-list/payout-list.component';
import { PayoutDetailsComponent } from './AdminModule/payout/payout-details/payout-details.component';
import { MobileBusinessManageProductComponent } from './UserModule/business-account/mobile-business-manage-account/mobile-business-manage-product/mobile-business-manage-product.component';
import { PrivacyViewComponent } from './CMSModule/privacy/privacy-view/privacy-view.component';
import { PrivacyListComponent } from './CMSModule/privacy/privacy-list/privacy-list.component';
import { HelpViewComponent } from './CMSModule/help/help-view/help-view.component';
import { EditContactProfileComponent } from './BookingModule/create-booking/edit-contact-profile/edit-contact-profile.component';
import { VoucherComponent } from './BookingModule/create-booking/voucher/voucher.component';
import { CreateVoucherComponent } from './AdminModule/create-voucher/create-voucher.component';
import { VoucherListComponent } from './AdminModule/create-voucher/voucher-list/voucher-list.component';
import { RefundPolicyViewComponent } from './CMSModule/refund-policy/refund-policy-view/refund-policy-view.component';
import { RefundPolicyListComponent } from './CMSModule/refund-policy/refund-policy-list/refund-policy-list.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ConnectionServiceModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatButtonToggleModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatDatepickerModule,
    MatAutocompleteModule,
    MatPaginatorModule,
    MatChipsModule,
    MatSortModule,
    MatStepperModule,
    MatProgressSpinnerModule,
    MatCheckboxModule,
    NgbModule,
    CalendarModule,
    TimePickerModule,
    DeferLoadModule,
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyCud7pOAvltknKlt2DknxF9djGxN_nsRbA",
      authDomain: "angular-file-upload-4ae5e.firebaseapp.com",
      storageBucket: "gs://angular-file-upload-4ae5e.appspot.com",
      projectId: "angular-file-upload-4ae5e",
    }),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyAmlnIAu52yOhMzIPFGxSsDaHB9TXmqhQk',
      libraries: ['places']
    }),
    DeviceDetectorModule.forRoot(),
    AngularFireStorageModule
  ],
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgotpasswordComponent,
    ProductComponent,
    ServiceComponent,
    CardServiceListComponent,
    CardServiceHotelComponent,
    CardProductListComponent,
    CardBookingListComponent,
    CustomerAccountComponent,
    MyBookingComponent,
    CustomerSidebarComponent,
    CreateBookingComponent,
    BookingDetailsComponent,
    BusinessAccountComponent,
    BusinessSidebarComponent,
    MyBookingDetailsComponent,
    BusinessOrderComponent,
    MainHeaderComponent,
    MainFooterComponent,
    BookingFooterComponent,
    BookingHeaderComponent,
    HomepageComponent,
    BusinessManageAccountComponent,
    BusinessBankAccountComponent,
    BusinessAccountProductComponent,
    BusinessAccountScheduleComponent,
    BusinessAccountPhotosComponent,
    HomepageSearchComponent,
    MainUspComponent,
    DownloadCTAComponent,
    EditProfileByIdComponent,
    MyscheduleComponent,
    CustomerEditProfileComponent,
    BuinessEditProfileComponent,
    DialogComponent,
    MobileBusinessManageAccountComponent,
    OrderListComponent,
    OrderDetailsComponent,
    TrackerComponent,
    BlockServiceListComponent,
    PayoutListComponent,
    PayoutDetailsComponent,
    MobileBusinessManageProductComponent,
    PrivacyViewComponent,
    PrivacyListComponent,
    HelpViewComponent,
    MobileBusinessManageProductComponent,
    EditContactProfileComponent,
    PrivacyListComponent,
    VoucherComponent,
    CreateVoucherComponent,
    VoucherListComponent,
    RefundPolicyViewComponent,
    RefundPolicyListComponent
  ],
  entryComponents: [ DialogComponent ],
   bootstrap: [ AppComponent ],
   providers: [
     {
       provide: HTTP_INTERCEPTORS,
       useClass: HttpErrorInterceptor,
       multi: true
     },
     {
      provide: LOCALE_ID, useValue: 'id'
     }, AuthGuard
   ],
   exports: [
     MatFormFieldModule,
     MatInputModule,
     MatIconModule,
     MatButtonModule,
     MatDialogModule,
     MatCheckboxModule,
     MatAutocompleteModule,
     MatButtonToggleModule,
     MatButtonModule,
     MatTableModule,
     MatChipsModule,
     MatSortModule,
     MatPaginatorModule,
     MatProgressSpinner,
     MatStepperModule
   ]
})
export class AppModule {
  constructor() {
    // Add an icon to the library for convenient access in other components
  }
 }