import { Component, OnInit, Input } from '@angular/core';
import { ServiceService } from '../../../../services/service.service';
import { Service } from '../../../../models/service';

@Component({
  selector: 'app-block-service-list',
  templateUrl: './block-service-list.component.html',
  styleUrls: ['./block-service-list.component.css']
})
export class BlockServiceListComponent implements OnInit {

  latitude: number;
  longitude: number;
  hotelService: Service;
  groomingService: Service;
  @Input() type: string;
  constructor(private servService: ServiceService) {}

  ngOnInit() {
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {

        this.latitude = +position.coords.latitude;
        this.longitude = +position.coords.longitude;

          if(this.type == 'hotel'){
            this.servService.getServiceTypeViewByCurrentLocation('hotel', this.latitude, this.longitude)
            .subscribe(hotelService => {
            this.hotelService = hotelService;
          });
          } else if(this.type == 'grooming'){
            this.servService.getServiceTypeViewByCurrentLocation('grooming', this.latitude, this.longitude)
            .subscribe(groomingService => {
            this.groomingService = groomingService;
          });
          }
      });
    }
  }

}
