import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router'
import { Service } from 'src/app/models/service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../components/shared/dialog/dialog.component';
import { AuthService } from '../../../services/auth.service';

@Component({
  selector: 'app-card-service-list',
  templateUrl: './card-service-list.component.html',
  styleUrls: ['./card-service-list.component.css']
})
export class CardServiceListComponent implements OnInit {

  GROOMINGBANNER = ("../../../../assets/image/grooming.jpg");
  HOTELBANNER = ("../../../assets/image/pet_hotel.jpg");
  BANNER;
  @Input() serviceList: Service;
  dialogRef: MatDialogRef<DialogComponent>;
  type;
  
  customerId: string;

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private dialog: MatDialog) { }

  ngOnInit() {
    this.type = this.route.snapshot.paramMap.get('type');
    if(this.type == "hotel"){
      this.BANNER = this.HOTELBANNER;
    } else if(this.type == "grooming"){
      this.BANNER = this.GROOMINGBANNER;
    }
  }

  setUserData(id_user: string){
    if(this.authService.getCustomerId() != null){
      this.customerId = this.authService.getCustomerId();
    } else {
      this.customerId = null;
    }
    
    this.customerId = this.authService.getCustomerId();
    if(id_user == this.customerId){
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Can't book your own Services"
        }
      });
    } else { 
      const type = this.route.snapshot.paramMap.get('type');
      if(type == null){
        this.router.navigate(['/product/id/' + id_user + '/service/type/' + this.serviceList.service_type]);
      } else {
        this.router.navigate(['/product/id/' + id_user + '/service/type/' + type]);
      }
    }
  }

}
