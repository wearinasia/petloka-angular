import { Component, OnInit, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';
import { ActivatedRoute } from '@angular/router';
import { Profile } from 'src/app/models/profile';
import { formatDate } from '@angular/common';
import { isString } from 'util';

@Component({
  selector: 'app-card-service-profile',
  templateUrl: './card-service-profile.component.html',
  styleUrls: ['./card-service-profile.component.css'],
  providers: [ NgbCarouselConfig ]
})
export class CardServiceHotelComponent implements OnInit {

  @Input() userChild: Profile;
  public images;
  public image;
  serviceType;
  GROOMINGBANNER = ("../../../../assets/image/grooming.jpg");
  HOTELBANNER = ("../../../assets/image/pet_hotel.jpg");

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.serviceType = this.route.snapshot.paramMap.get('type');
    if(this.userChild.host.grooming.operationalHours.weekDays.start.match(/^([0-1]\d):([0-5]\d)\s?(?:AM|PM)?$/i)){
      
    }else{
      this.userChild.host.grooming.operationalHours.weekDays.start = formatDate(this.userChild.host.grooming.operationalHours.weekDays.start, 'h:mm a', 'id', '+0700');
      this.userChild.host.grooming.operationalHours.weekDays.end = formatDate(this.userChild.host.grooming.operationalHours.weekDays.end, 'h:mm a', 'id', '+0700');
      this.userChild.host.hotel.operationalHours.weekDays.start = formatDate(this.userChild.host.hotel.operationalHours.weekDays.start, 'h:mm a', 'id', '+0700');
      this.userChild.host.hotel.operationalHours.weekDays.end = formatDate(this.userChild.host.hotel.operationalHours.weekDays.end, 'h:mm a', 'id', '+0700');
    }
  }
}
