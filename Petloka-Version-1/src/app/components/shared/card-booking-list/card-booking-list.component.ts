import { Component, OnInit, Input } from '@angular/core';
import { MyBooking } from 'src/app/models/mybooking';
import { formatDate } from '@angular/common';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-card-booking-list',
  templateUrl: './card-booking-list.component.html',
  styleUrls: ['./card-booking-list.component.css']
})
export class CardBookingListComponent implements OnInit {

  @Input() bookingList: MyBooking;
  faChevronRight = faChevronRight;
  title: string;
  constructor(private pageTitle: Title, private router: Router) { }

  ngOnInit() {
    this.title = this.pageTitle.getTitle();
    if(this.bookingList.start_time != null){
      this.bookingList.start_time = formatDate(this.bookingList.start_time, 'shortDate', 'id', '+0700')
    }
  }

  toBookingDetails(bId){
    if(this.title == 'Customer Booking'){
      this.router.navigate(['/account/customer/booking/details/id/'+bId]);
    } else if(this.title == 'Business Order'){
      this.router.navigate(['/account/business/orders/details/id/'+bId]);
    }
  }

}
