import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from 'src/app/services/service.service';
import { Product } from 'src/app/models/product';
import { ProductService } from '../../../services/product.service';
import { Title } from '@angular/platform-browser';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../components/shared/dialog/dialog.component';

@Component({
  selector: 'app-card-product-list',
  templateUrl: './card-product-list.component.html',
  styleUrls: ['./card-product-list.component.css']
})
export class CardProductListComponent implements OnInit {

  @Input() productList: Product;
  @Output() public refresh: EventEmitter<any> = new EventEmitter();
  dialogRef: MatDialogRef<DialogComponent>;
  uId: string;
  discountedPrice: number;

  constructor(private router: Router, private servService: ServiceService, public title: Title, private prodService: ProductService, private dialog: MatDialog, private route: ActivatedRoute) {}

  ngOnInit(){
    this.discountedPrice = this.productList.price - this.productList.deducted_price;
    this.uId = this.route.snapshot.paramMap.get('uId');
  }

  toBooking(pId: string, serviceType: string){
    this.router.navigate(['createbooking'], {queryParams : {step : 1}});
    localStorage.setItem('cartSession', JSON.stringify({ pId : pId}));
    this.servService.setServiceType(serviceType);
  }

  delete(pId: string){
    this.prodService.deleteProductBypId(pId)
    .subscribe(deleteProduct => {
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: this.productList.product_name + " has been deleted successfully"
        }
      });
      this.refresh.emit();
      this.router.navigate(['account/business/manage-service']);
      if(this.title.getTitle() == 'Mobile Manage Account'){
        this.router.navigate(['account/business/manage-service/' + this.uId])
      }
    });
  }

  edit(pId: string,serviceType: string){
    this.router.navigate(['/account/business/manage-service'], {queryParams: {type: serviceType,pId: pId, steps: 1}});
  }

  editMobile(pId: string,serviceType: string){
    this.router.navigate(['/account/business/manage-service/' + this.route.snapshot.paramMap.get('uId')], {queryParams: {type: serviceType,pId: pId}});
  }

}
