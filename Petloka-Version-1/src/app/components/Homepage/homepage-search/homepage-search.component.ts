import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { LocationService } from '../../../services/location.service';
import { Location } from '../../../models/location';

@Component({
  selector: 'app-homepage-search',
  templateUrl: './homepage-search.component.html',
  styleUrls: ['./homepage-search.component.css']
})
export class HomepageSearchComponent implements OnInit {

  
  matcher: string;
  serviceType      = new FormControl('');
  // locations: string[] = ['Bali', 'Jakarta Barat', 'Jakarta Pusat',
  //                        'Jakarta Selatan', 'Jakarta Timur', 'Tangerang', 'Tangerang Selatan',
  //                        'Jakarta Utara', 'Bandung', 'Depok', 'Bekasi', 'Surabaya',
  //                        'Yogyakarta', 'Malang', 'Semarang', 'Bogor', 'Solo', 'Tanah Bumbu'];
  locationsControl = new FormControl('');
  filteredLocations: Observable<Location[]>
  locations: Location[];
  

  constructor(private router: Router, private route: ActivatedRoute, public locService: LocationService) { }

  ngOnInit() {
    this.locService.getCityList()
    .subscribe(location => {
          this.locations = location;
          this.filteredLocations = this.locationsControl.valueChanges
          .pipe(
            startWith(''),
            map(value => this._filter(value, location))
          )
    })
  }
  
  private _filter(value: string, location): Location[]{
    const filterValue = value.toLowerCase();

    return location.filter(location => location.name.toLowerCase().includes(filterValue));
  }

  toSearchResult(){
    
    if (this.locService.getMatcher() == "Ok") {
      this.router.navigate(['/service/type/' + this.serviceType.value], {queryParams: {location: this.locationsControl.value}})
    }
    else if(this.locService.getMatcher() == "Error") {
      this.matcher = "Error";
    }
  }
  
}

