import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private router: Router, private pageTitle: Title) { }

  HOTELBANNER = ("../../../assets/image/pet_hotel.jpg");
  GROOMINGBANNER = ("../../../assets/image/grooming.jpg");

  ngOnInit() {
    this.pageTitle.setTitle("Home");
  }

  toHotel(){
    this.router.navigate(['/service/type/hotel'])
  }

  toGrooming(){
    this.router.navigate(['/service/type/grooming']);
  }

}
