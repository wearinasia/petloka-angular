import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../../../services/booking.service';
import { SalesService } from '../../../../services/sales.service';
import { ActivatedRoute } from '@angular/router';
import { formatDate } from '@angular/common';
import { BookingDetails } from '../../../../models/bookingdetails';
import { AuthService } from '../../../../services/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-order-details',
  templateUrl: './order-details.component.html',
  styleUrls: ['./order-details.component.css']
})
export class OrderDetailsComponent implements OnInit {

  private bId: string;
  orderDetails: BookingDetails;
  orderStatus: string;
  total: number;
  constructor(private pageTitle: Title, private authService: AuthService, private bookingService: BookingService, private route: ActivatedRoute, private salesService: SalesService) { }

  ngOnInit() {
    this.pageTitle.setTitle('Order Details');
    this.bId = this.route.snapshot.paramMap.get('bId');
    this.bookingService.myBookingDetails(this.bId)
    .subscribe(orderDetails => {
      if(orderDetails.deduction > orderDetails.product.price){
        orderDetails.deduction = orderDetails.product.price;
        this.total = (orderDetails.product.price - orderDetails.deduction) + orderDetails.transport_fee;
      }else {
        this.total = (orderDetails.product.price - orderDetails.deduction) + orderDetails.transport_fee;
      }
      // orderDetails.start_time = formatDate(orderDetails.start_time, 'MMM d, y || h:mm:ss', 'id', '+0700');
      this.orderDetails = orderDetails;

      if(orderDetails.status == 'PAYMENT_CONFIRMATION'){
        this.orderStatus = 'Pending Payment';
      } else if(orderDetails.status == 'HOST_NOTIFIED'){
        this.orderStatus = 'On Process';
      } else if(orderDetails.status == 'COMPLETED') {
        this.orderStatus = 'Completed';
      }
    })
  }

  changeStatus(bId: string, status: string){
    this.salesService.changeStatus(bId, {"status": status})
    .subscribe(response => {
      if(response.status == 'PAYMENT_CONFIRMATION'){
        this.orderStatus = 'Pending Payment';
      } else if(response.status == 'HOST_NOTIFIED'){
        this.orderStatus = 'On Process';
        this.salesService.updateStatus(bId, "PAYMENT_SUCCESS");
      } else if(response.status == 'COMPLETED'){
        this.orderStatus = 'Completed';
      }
    })
  }

}
