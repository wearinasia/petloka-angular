import { Component, OnInit, ViewChild } from '@angular/core';
import { SalesService } from '../../../../services/sales.service';
import { formatDate } from '@angular/common';
import { OrderList } from '../../../../models/orderlist';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['./order-list.component.css']
})
export class OrderListComponent implements OnInit {

  // orderList: OrderList[];
  orderList: MatTableDataSource<OrderList>;
  displayedColumns: string[] = ['booking_id', 'contact_name', 'booking_date', 'status', 'action'];
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private salesService: SalesService, private router: Router, private pageTitle: Title) { }

  ngOnInit() {
    this.pageTitle.setTitle('Admin');
    this.salesService.getAllOrder().
    subscribe(orderList =>{
        for(let i = 0; i < orderList.length; i++){
          if(orderList[i].start_time == "8, 9, 2019T15:16"){
          }else{
            orderList[i].start_time =  formatDate(orderList[i].start_time, 'MMM d, y', 'id', '+0700');
          }
          if(orderList[i].status == 'PAYMENT_CONFIRMATION'){
            orderList[i].status = 'Pending Payment';
          } else if(orderList[i].status == 'HOST_NOTIFIED'){
            orderList[i].status= 'On Process';
          } else {
            orderList[i].status = 'Completed';
          }
        }
        orderList.sort((a, b) => b.booking_id.localeCompare(a.booking_id));
        this.orderList = new MatTableDataSource(orderList);
        this.orderList.paginator = this.paginator;
        this.orderList.sort = this.sort;
    })
  }

  orderDetails(bId: string){
    window.open('/admin/sales/order-details/id/' + bId, '_blank');
    // this.router.navigate(['/admin/sales/order-details/id/' + bId]);
  }

  onRightClick(event){
    console.log(event);
  }

  applyFilter(filterValue: string) {
    this.orderList.filter = filterValue.trim().toLowerCase();
  }
}
