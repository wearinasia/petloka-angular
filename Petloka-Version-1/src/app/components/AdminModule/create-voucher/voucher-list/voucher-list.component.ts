import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Title } from '@angular/platform-browser';
import { Voucher } from '../../../../models/voucher';
import { VoucherService } from '../../../../services/voucher.service';

@Component({
  selector: 'app-voucher-list',
  templateUrl: './voucher-list.component.html',
  styleUrls: ['./voucher-list.component.css']
})
export class VoucherListComponent implements OnInit {

  displayedColumns: string[] = ['id', 'rule_name', 'value', 'rule_type', 'action'];
  voucherList: MatTableDataSource<Voucher>;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private pageTitle: Title, private voucherService: VoucherService) { }

  ngOnInit() {
    this.pageTitle.setTitle('Voucher List');
    this.voucherService.getVoucherRulesList()
    .subscribe(voucherList => {
      this.voucherList = new MatTableDataSource(voucherList);
      this.voucherList.paginator = this.paginator;
      this.voucherList.sort = this.sort;
    })
  }

  applyFilter(filterValue: string) {
    this.voucherList.filter = filterValue.trim().toLowerCase();
  }

}
