import { Component, OnInit, ViewChild } from '@angular/core';
import { PayoutService } from '../../../../services/payout.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { PayoutList } from '../../../../models/payoutlist';
import { formatDate } from '@angular/common';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-payout-list',
  templateUrl: './payout-list.component.html',
  styleUrls: ['./payout-list.component.css']
})
export class PayoutListComponent implements OnInit {

  payoutList: MatTableDataSource<PayoutList>;
  displayedColumns: string[] = ['payout_id', 'booking_id', 'host_name', 'payout_date', 'status', 'action'] 
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private payoutService: PayoutService, private pageTitle: Title, private router: Router) {}

  ngOnInit() {
    this.pageTitle.setTitle('Admin');
    this.payoutService.getPayoutList()
    .subscribe(payoutList => {
      for(let i = 0; i < payoutList.length; i++){
        if(payoutList[i].booking.start_time == "8, 9, 2019T15:16"){
        }else{
        payoutList[i].booking.start_time =  formatDate(payoutList[i].booking.start_time, 'MMM d, y', 'id', '+0700');
        }
      }
      payoutList.sort((a, b) => b.booking.booking_id.localeCompare(a.booking.booking_id));
      this.payoutList = new MatTableDataSource(payoutList);
      this.payoutList.paginator = this.paginator;
      this.payoutList.sort = this.sort;
      this.payoutList.filterPredicate = (data: any, filter) => { 
        const dataStr = JSON.stringify(data).toLowerCase();
        return dataStr.indexOf(filter) != -1; }
    })
  }

  payoutDetails(bId: string){
    window.open('/admin/payout/payout-details/id/' + bId, '_blank');
  }

  applyFilter(filterValue: string) {
    this.payoutList.filter = filterValue.trim().toLowerCase();
  }

}
