import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PayoutService } from '../../../../services/payout.service';
import { PayoutList } from '../../../../models/payoutlist';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-payout-details',
  templateUrl: './payout-details.component.html',
  styleUrls: ['./payout-details.component.css']
})
export class PayoutDetailsComponent implements OnInit {

  private bId: string;
  payoutDetails: PayoutList;
  total: number;
  constructor(private payoutService: PayoutService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.bId = this.route.snapshot.paramMap.get('bId');
    this.payoutService.getPayoutbyId(this.bId)
    .subscribe(payoutDetails => {
      if(payoutDetails.booking.deduction > payoutDetails.product.price){
        payoutDetails.booking.deduction = payoutDetails.product.price;
        this.total = (payoutDetails.product.price - payoutDetails.booking.deduction) + payoutDetails.booking.transport_fee;
      }else {
        this.total = (payoutDetails.product.price - payoutDetails.booking.deduction) + payoutDetails.booking.transport_fee;
      }
      this.payoutDetails = payoutDetails;
      // payoutDetails.booking.start_time = formatDate(payoutDetails.booking.start_time, 'MMM d, y || h:mm:ss', 'id', '+0700'); 
    })
  }

  changeStatus(bId: string, status){
    this.payoutService.changePayoutStatus(bId, {"status": status})
    .subscribe(response => {
    })
  }

}
