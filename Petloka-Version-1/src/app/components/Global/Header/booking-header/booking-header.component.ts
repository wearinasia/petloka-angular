import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-booking-header',
  templateUrl: './booking-header.component.html',
  styleUrls: ['./booking-header.component.css']
})
export class BookingHeaderComponent implements OnInit {

  //Font Awesome
  faChevronRight = faChevronRight;
  faArrowLeft = faArrowLeft;

  constructor(private location: Location) { }

  ngOnInit() {
  }

  back(){
    this.location.back();
  }

}
