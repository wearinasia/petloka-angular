import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../../../services/auth.service';
import { faBriefcase } from '@fortawesome/free-solid-svg-icons';
import { faCalendarDay } from '@fortawesome/free-solid-svg-icons';
import { faUser } from '@fortawesome/free-solid-svg-icons';
import { faArrowLeft } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.css']
})
export class MainHeaderComponent implements OnInit {
  LOGO = ("../../../assets/image/petloka-logo-blue.png");
  customerSession = JSON.parse(localStorage.getItem("customerSession"));
  isLoggedIn: boolean;

  //Font Awesome
  faBriefcase = faBriefcase;
  faCalendarDay = faCalendarDay;
  faUser = faUser;
  faArrowLeft = faArrowLeft;

  constructor(private authService: AuthService, private router: Router, private location: Location, public pageTitle: Title) { }

  ngOnInit() {
    this.isLoggedIn = this.authService.getLoggedIn();
  }

  logout(){
    this.authService.logout();
    this.router.navigateByUrl('');
    if(this.pageTitle.getTitle() == 'Home'){
      location.reload();
    }
  }

  back(){
    this.location.back();
  }

}
