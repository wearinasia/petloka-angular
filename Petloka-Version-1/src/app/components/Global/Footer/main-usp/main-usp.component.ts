import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-usp',
  templateUrl: './main-usp.component.html',
  styleUrls: ['./main-usp.component.css']
})
export class MainUspComponent implements OnInit {

  
  public VERIFIED_IMG = ("../../../assets/image/verified.png");
  public PRACTICAL_IMG = ("../../../assets/image/practical.png");
  public INSURANCE_IMG = ("../../../assets/image/insurance.png");
  constructor() { }

  ngOnInit() {
  }

}
