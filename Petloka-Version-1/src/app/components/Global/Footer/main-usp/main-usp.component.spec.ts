import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainUspComponent } from './main-usp.component';

describe('MainUspComponent', () => {
  let component: MainUspComponent;
  let fixture: ComponentFixture<MainUspComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MainUspComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainUspComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
