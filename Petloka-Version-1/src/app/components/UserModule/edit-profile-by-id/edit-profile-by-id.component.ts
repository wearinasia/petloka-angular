import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../../../services/profile.service';
import { ProductService } from '../../../services/product.service';
import { Validators, FormControl, FormGroup, FormArray } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { LocationService } from '../../../services/location.service';
import { Location } from '../../../models/location';
import { MapsAPILoader } from '@agm/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../components/shared/dialog/dialog.component';

@Component({
  selector: 'app-edit-profile-by-id',
  templateUrl: './edit-profile-by-id.component.html',
  styleUrls: ['./edit-profile-by-id.component.css']
})
export class EditProfileByIdComponent implements OnInit {

  profileByIdForm: FormGroup;
  filteredLocations: Observable<Location[]>;
  dialogRef: MatDialogRef<DialogComponent>;
  locations: Location[];
  matcher: string;
  password: string = '8470311';

  latitude: number = -6.175387099999986;
  longitude: number = 106.82714748255614;
  zoom: number;
  address: string;
  private geoCoder;
  @ViewChild('search', {static: false})
  public searchElementRef: ElementRef;
  
  userId = new FormControl('');
  
  constructor(private route: ActivatedRoute, private dialog: MatDialog, private prodService: ProductService, private profileService: ProfileService, private router: Router, public locService: LocationService, private ngZone: NgZone, private mapsAPILoader: MapsAPILoader) {
    this.initializeForm();
  }

  ngOnInit() {
    if(this.route.snapshot.paramMap.get('password') != this.password){
      this.router.navigateByUrl('');
    }
    this.profileByIdForm.patchValue({
      geolocation:{
        type: 'Point'
      }
    })
    this.locService.getCityList()
        .subscribe(location => {
              this.locations = location;
              this.filteredLocations = this.profileByIdForm.get('coverage_area').get('city').valueChanges
              .pipe(
                startWith(''),
                map(value => this.locService._filter(value, location))
              )
    })

    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;
 
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
 
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.getAddress(this.latitude, this.longitude);
          this.addCoordinates(this.longitude, this.latitude);
          this.zoom = 12;
        });
      });
    });

  }

  profileDetails(e){
    this.prodService.getUserDetails(e.target.value)
    .subscribe(userDetails => {
      if(userDetails.host.grooming.geolocation.coordinates != null){
        this.longitude = +userDetails.host.grooming.geolocation.coordinates[0];
        this.latitude = +userDetails.host.grooming.geolocation.coordinates[1];
        this.getAddress(this.latitude, this.longitude);
        this.profileByIdForm.patchValue(userDetails.host.grooming);
        } else {
          this.profileByIdForm.patchValue({
            geolocation: {
              type: "Point",
              coordinates: ["", ""]
            },
            phone_number: userDetails.host.grooming.phone_number,
            host_name:   userDetails.host.grooming.host_name,
            is_company:  userDetails.host.grooming.is_company,
               coverage_area:     {
                  city:  userDetails.host.grooming.coverage_area.city
               },
            tagline:     userDetails.host.grooming.tagline
          });
        }
    })
  }

  send(){

    this.profileService.editProfileNoLogin(this.userId.value, 'hotel', this.profileByIdForm.value)
    .subscribe(editProfile => {
    });
    this.profileService.editProfileNoLogin(this.userId.value, 'grooming', this.profileByIdForm.value)
    .subscribe(editProfile => {
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Profile Updated"
        }
      });
    })
  }

  initializeForm(){
    this.profileByIdForm =  new FormGroup({
      host_name:            new FormControl(''),
      phone_number:         new FormControl(null,[Validators.pattern(/^-?(0|[0-9]\d*)?$/)]),
      coverage_area:        new FormGroup({
        city:               new FormControl('')
        // region:             new FormControl('')
      }),
      tagline:              new FormControl('', [Validators.maxLength(100)]),
      geolocation:          new FormGroup({
        type:               new FormControl(''),
        coordinates:        new FormArray([
          new FormControl(),
          new FormControl()
        ])
      }),
      total_rating:         new FormControl(null,[Validators.pattern(/^\-?\d+((\.|\,)\d+)?$/)])
    })
  }

  get globalCoordinates(){
    return this.profileByIdForm.get('geolocation').get('coordinates') as FormArray;
  }

  addCoordinates(longitude, latitude){
    this.globalCoordinates.patchValue([longitude, latitude]);
  }

  private setCurrentLocation(){
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        // this.latitude = position.coords.latitude;
        // this.longitude = position.coords.longitude;
          this.longitude = 106.82714748255614;
          this.latitude = -6.175387099999986;
          this.getAddress(this.latitude, this.longitude);
          this.addCoordinates(this.longitude, this.latitude);
          this.zoom = 15;
        
      });
    }
  }

  markerDragEnd(event) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
    this.addCoordinates(this.longitude, this.latitude);
  }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }

}
