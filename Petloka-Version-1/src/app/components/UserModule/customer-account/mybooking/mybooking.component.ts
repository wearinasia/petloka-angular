import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../../../services/booking.service';
import { MyBooking } from '../../../../models/mybooking';
import { AuthService } from '../../../../services/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-mybooking',
  templateUrl: './mybooking.component.html',
  styleUrls: ['./mybooking.component.css']
})
export class MyBookingComponent implements OnInit {

  myBookings : MyBooking[];

  constructor(private pageTitle: Title, private bookingService: BookingService, private authService: AuthService) { }

  ngOnInit() {
    this.pageTitle.setTitle('Customer Booking');
    this.bookingService.getMyBooking()
    .subscribe(myBooking => {
      myBooking.sort((a, b) => a < b ? 1 : -1);
      this.myBookings = myBooking;
    })
  }

}
