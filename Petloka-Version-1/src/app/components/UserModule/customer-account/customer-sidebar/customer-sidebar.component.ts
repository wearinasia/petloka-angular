import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../../services/profile.service';
import { AuthService } from '../../../../services/auth.service';
import { Profile } from '../../../../models/profile';
import { Router } from '@angular/router';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-customer-sidebar',
  templateUrl: './customer-sidebar.component.html',
  styleUrls: ['./customer-sidebar.component.css']
})
export class CustomerSidebarComponent implements OnInit {

  profile: Profile;
  
  //Font Awesome
  faChevronRight = faChevronRight;

  constructor(private authService: AuthService, private profileService: ProfileService, private router: Router) { }

  ngOnInit() {
    this.profileService.getProfileDetails()
    .subscribe(profileDetails => {
      this.profile = profileDetails;
    })
  }

}
