import { Component, OnInit } from '@angular/core';
import { BookingService } from '../../../../services/booking.service';
import { ProductService } from '../../../../services/product.service';
import { ActivatedRoute } from '@angular/router';
import { BookingDetails } from '../../../../models/bookingdetails';
import { formatDate } from '@angular/common';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-mybooking-details',
  templateUrl: './mybooking-details.component.html',
  styleUrls: ['./mybooking-details.component.css']
})
export class MyBookingDetailsComponent implements OnInit {

  bookingDetails : BookingDetails;
  startTime: string;
  endTime: string;
  total: number;

  constructor(private authService: AuthService, private bookingService : BookingService, private route : ActivatedRoute, private prodService: ProductService) { }

  ngOnInit() {
    const bId = this.route.snapshot.paramMap.get('bId');
    this.bookingService.myBookingDetails(bId)
    .subscribe(myBookingDetails => {
      if(myBookingDetails.deduction > myBookingDetails.product.price){
        myBookingDetails.deduction = myBookingDetails.product.price;
        this.total = (myBookingDetails.product.price - myBookingDetails.deduction) + myBookingDetails.transport_fee;
      }else {
        this.total = (myBookingDetails.product.price - myBookingDetails.deduction) + myBookingDetails.transport_fee;
      }
      this.bookingDetails = myBookingDetails;
      this.total = myBookingDetails.product.price - myBookingDetails.deduction + myBookingDetails.transport_fee;
      this.startTime = formatDate(myBookingDetails.start_time, 'shortDate', 'id', '+0700');
      if(myBookingDetails.end_time != ""){
      this.endTime = formatDate(myBookingDetails.end_time, 'shortDate', 'id', '+0700');
      }
    })
  }

}
