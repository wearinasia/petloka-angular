import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AuthService } from '../../../services/auth.service';
import { faChevronRight} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-customer-account',
  templateUrl: './customer-account.component.html',
  styleUrls: ['./customer-account.component.css']
})
export class CustomerAccountComponent implements OnInit {

  faChevronRight = faChevronRight;
  constructor(private pageTitle: Title) {
  }

  ngOnInit() {
    this.pageTitle.setTitle('Manage Customer Account');
  }
}
