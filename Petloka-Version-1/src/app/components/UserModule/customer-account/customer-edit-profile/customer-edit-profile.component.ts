import { Component, OnInit } from '@angular/core';
import { Profile } from 'src/app/models/profile';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { ProfileService } from 'src/app/services/profile.service';
import { AuthService } from '../../../../services/auth.service';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { LocationService } from '../../../../services/location.service';
import { Location } from '../../../../models/location';
import { DialogComponent } from '../../../../components/shared/dialog/dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

export const CUSTOM_FORMATS = {
  parse: {
    dateInput: 'LL',
  },
  display: {
    dateInput: 'LL',
    monthYearLabel: 'MMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY',
  },
}

@Component({
  selector: 'app-customer-edit-profile',
  templateUrl: './customer-edit-profile.component.html',
  styleUrls: ['./customer-edit-profile.component.css'],
  providers: [
    {provide: MAT_DATE_LOCALE, useValue: 'id'},
    {provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE]},
    {provide: MAT_DATE_FORMATS, useValue: CUSTOM_FORMATS}
  ]
})
export class CustomerEditProfileComponent implements OnInit {

  dialogRef: MatDialogRef<DialogComponent>;
  profile: Profile;
  profileForm: FormGroup;
  earlyForm: any;

  filteredLocations: Observable<Location[]>;
  locations: Location[];
  matcher: string;

  constructor(private authService: AuthService, private dialog: MatDialog, private profileService: ProfileService, private locService: LocationService) { 
    this.initializeForm();
  }

  ngOnInit() {
    this.profileService.getProfileDetails()
    .subscribe(profileDetails => {
      this.profile = profileDetails;
      this.profileForm.patchValue(profileDetails);
      this.earlyForm = this.profileForm.value;

      this.locService.getCityList()
        .subscribe(location => {
              this.locations = location;
              this.filteredLocations = this.profileForm.get('address').get('city').valueChanges
              .pipe(
                startWith(''),
                map(value => this.locService._filter(value, location))
              )
        })

    })
  }

  submit(){
    const form = this.profileForm.value;
    if(form != this.earlyForm){
      this.profileService.editProfile(form)
      .subscribe(editProfile => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: "Profile Updated"
          }
        });
      
    })
    } else {
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Profile Unchanged"
        }
      });
    }
  }

  initializeForm(){
    this.profileForm = new FormGroup({
      full_name:    new FormControl('' , [Validators.required, Validators.pattern(/^[a-z A-Z]*$/)]),
      email:        new FormControl('', [Validators.required, Validators.email]),
      address:      new FormGroup({
        street:     new FormControl(''),
        building:   new FormControl(''),
        city:       new FormControl(''),
        region:     new FormControl('')
      }),
      phone_number:     new FormControl('', [Validators.required, Validators.pattern(/^-?(0|[0-9]\d*)?$/)]),
      photo_url:        new FormControl(''),
      about_me:         new FormControl('', [Validators.maxLength(100)]),
      birth_date:   new FormControl(''),
      verified:     new FormControl('')
    })
  }
}

