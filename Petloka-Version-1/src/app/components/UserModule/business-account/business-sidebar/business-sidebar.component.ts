import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProfileService } from '../../../../services/profile.service';
import { Profile } from '../../../../models/profile';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-business-sidebar',
  templateUrl: './business-sidebar.component.html',
  styleUrls: ['./business-sidebar.component.css']
})
export class BusinessSidebarComponent implements OnInit {

  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  customerId: string;
  idToken: string;
  profile: Profile;

  //Font Awesome
  faChevronRight = faChevronRight;

  constructor(private profileService: ProfileService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {
  }

}
