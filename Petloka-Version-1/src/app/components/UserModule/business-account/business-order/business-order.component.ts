import { Component, OnInit } from '@angular/core';
import { MyBooking } from '../../../../models/mybooking';
import { BookingService } from '../../../../services/booking.service';
import { AuthService } from '../../../../services/auth.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-business-order',
  templateUrl: './business-order.component.html',
  styleUrls: ['./business-order.component.css']
})
export class BusinessOrderComponent implements OnInit {
  
  myOrders : MyBooking[];

  constructor(private bookingService : BookingService, private authService: AuthService, private pageTitle: Title) { }

  ngOnInit() {
    this.pageTitle.setTitle('Business Order');
    this.bookingService.getMyOrder()
    .subscribe(myBooking => {
      for(let i = 0;i < myBooking.length;i++){
        if(myBooking[i].status == 'PAYMENT_CONFIRMATION'){
          myBooking[i].status = 'Pending Payment';
        } else if(myBooking[i].status == 'HOST_NOTIFIED'){
          myBooking[i].status= 'On Process';
        } else {
          myBooking[i].status = 'Completed';
        }
      }
      myBooking.sort((a, b) => a < b ? 1 : -1);
      this.myOrders = myBooking;

      
    })
  }
}
