import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { ProductService } from '../../../../services/product.service';
import { Product } from '../../../../models/product';

@Component({
  selector: 'app-mobile-business-manage-account',
  templateUrl: './mobile-business-manage-account.component.html',
  styleUrls: ['./mobile-business-manage-account.component.css']
})
export class MobileBusinessManageAccountComponent implements OnInit {

  uId: string;
  type: string;
  hotelProducts: Product[];
  groomProducts: Product[];
  constructor(private router: Router, private route: ActivatedRoute, private pageTitle: Title, private prodService: ProductService) { }

  ngOnInit() {
    this.pageTitle.setTitle('Mobile Manage Account');
    this.uId = this.route.snapshot.paramMap.get('uId');
    this.route.queryParams.subscribe(params => {
      this.type = params.type;
    })

    this.prodService.getProductList(this.uId,"hotel")
      .subscribe(
          hotelProduct => {
          return this.hotelProducts = hotelProduct;
        });
      this.prodService.getProductList(this.uId,"grooming")
        .subscribe(
          groomProduct => {
          return this.groomProducts = groomProduct;
        });
  }

  refreshData(){
    this.prodService.getProductList(this.uId,"hotel")
    .subscribe(
        hotelProduct => {
        return this.hotelProducts = hotelProduct;
      })
    this.prodService.getProductList(this.uId,"grooming")
      .subscribe(
        groomProduct => {
        return this.groomProducts = groomProduct;
      })
  }

  grooming(){
      this.router.navigate(['account/business/manage-service/' + this.uId], {queryParams: {type: 'grooming'}});
  }

  hotel(){
    this.router.navigate(['account/business/manage-service/' + this.uId], {queryParams: {type: 'hotel'}});
  }

}
