import { Component, OnInit } from '@angular/core';
import { Validators, FormArray, FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../../../../../services/product.service';
import { ProfileService } from '../../../../../services/profile.service';
import { MobileBusinessManageAccountComponent } from '../mobile-business-manage-account.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../../../components/shared/dialog/dialog.component';


@Component({
  selector: 'app-mobile-business-manage-product',
  templateUrl: './mobile-business-manage-product.component.html',
  styleUrls: ['./mobile-business-manage-product.component.css']
})
export class MobileBusinessManageProductComponent implements OnInit {

  globalProductForm: FormGroup;
  pId: string;
  type: string;
  dialogRef: MatDialogRef<DialogComponent>;
  constructor(private profileService: ProfileService, private dialog: MatDialog, private mobileBusiness: MobileBusinessManageAccountComponent, private route: ActivatedRoute, private prodService: ProductService) { 
    this.initializeGlobalProductForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.type = params.type;
      if(this.type == "hotel"){
        this.type = "Hotel";
        this.globalProductForm.reset();
        if(this.globalProductIncluding.length > 1){
          while(this.globalProductIncluding.length > 1){
            this.globalProductIncluding.removeAt(0);
          }
        }
      }
      else if(this.type == "grooming"){
        this.type = "Grooming";
        this.globalProductForm.reset();
        if(this.globalProductIncluding.length > 1){
          while(this.globalProductIncluding.length > 1){
            this.globalProductIncluding.removeAt(0);
          }
        }
      }
    })

    this.route.queryParams.subscribe(params => {
      this.pId = params.pId;
      if(this.pId != null){
        this.prodService.getProductDetails(this.pId)
        .subscribe(productDetails => {
          this.globalProductForm.patchValue(productDetails);
          for(let i = 0;i < productDetails.including.length; i++){
            this.setProduct(i, productDetails.including[i]);
          }
        })
      }
    }) 
  }

  saveGlobalProduct(){
    if(this.pId == null){

      if(this.type == "Hotel"){
        this.profileService.createProductHotel(this.globalProductForm.value)
        .subscribe(async editProfile => {
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: "Product Updated"
            }
          });
        this.globalProductForm.reset();
        this.mobileBusiness.refreshData();
        })
      }
      else if(this.type == "Grooming"){
        this.profileService.createProductGroom(this.globalProductForm.value)
        .subscribe(async editProfile => {
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: "Product Updated"
            }
          });
        this.globalProductForm.reset();
        this.mobileBusiness.refreshData();
        })
      }

    } else if (this.pId != null){
      this.prodService.editProductBypId(this.globalProductForm.value, this.pId)
      .subscribe(editProfile => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: "Product Edited"
          }
        });
        this.mobileBusiness.refreshData();
      })
    }
  }

  initializeGlobalProductForm(){
    this.globalProductForm =       new FormGroup({
      about:          new FormControl(''),
      price:          new FormControl(null, [Validators.pattern(/^-?(0|[0-9]\d*)?$/)]),
      product_name:   new FormControl('', [Validators.required]),
      including:      new FormArray([
        new FormControl('')
      ])
    })
  }

  get globalProductIncluding(){
    return this.globalProductForm.get('including') as FormArray;
  }

  addGlobalProductIncluding(){
    this.globalProductIncluding.push(new FormControl(''));
  }

  setProduct(i, items){
    this.globalProductIncluding.setControl(i,new FormControl(items));
  }

  deleteIncluding(index: number){
    this.globalProductIncluding.removeAt(index);
  }

}
