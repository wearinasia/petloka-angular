import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../../../../services/profile.service';
import { AuthService } from '../../../../services/auth.service';
import { Profile } from 'src/app/models/profile';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { DialogComponent } from '../../../../components/shared/dialog/dialog.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-business-bank-account',
  templateUrl: './business-bank-account.component.html',
  styleUrls: ['./business-bank-account.component.css']
})
export class BusinessBankAccountComponent implements OnInit {

  dialogRef: MatDialogRef<DialogComponent>;
  profile: Profile;
  bankForm: FormGroup;
  earlyForm: any;

  constructor(private authService: AuthService, private dialog: MatDialog, private profileService: ProfileService) {
    this.initializeForm();
  }

  ngOnInit() {
    this.profileService.getProfileDetails()
    .subscribe(profileDetails => {

      this.profile = profileDetails;
      this.bankForm.patchValue(profileDetails);
      this.earlyForm = this.bankForm.value;

    })
  }

  submit(){
    const form = this.bankForm.value;
    if(form != this.earlyForm) {
      this.profileService.editProfile(form)
      .subscribe(editProfile => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: "Profile Updated"
          }
        });
    })
    } else {
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Profile Unchanged"
        }
      });
    }
  }

  initializeForm(){
    this.bankForm =     new FormGroup({
      bank_info:        new FormGroup({
        // bank_id:        new FormControl(''),
        bank_name:      new FormControl(''),
        account_name:   new FormControl(''),
        account_number: new FormControl('', [Validators.required, Validators.pattern(/^-?(0|[0-9]\d*)?$/)])
      })
    })
  }

}
