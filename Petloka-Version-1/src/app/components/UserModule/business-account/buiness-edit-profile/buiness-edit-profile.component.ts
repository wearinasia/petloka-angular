import { Component, OnInit, ViewChild, ElementRef, NgZone } from '@angular/core';
import { Validators, FormControl, FormGroup, FormArray } from '@angular/forms';
import { Profile } from 'src/app/models/profile';
import { ProfileService } from 'src/app/services/profile.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { LocationService } from '../../../../services/location.service';
import { Location } from '../../../../models/location';
import { DialogComponent } from '../../../../components/shared/dialog/dialog.component';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { MapsAPILoader, MouseEvent } from '@agm/core';
import { Title } from '@angular/platform-browser';
import { DeviceDetectorService } from 'ngx-device-detector';

@Component({
  selector: 'app-buiness-edit-profile',
  templateUrl: './buiness-edit-profile.component.html',
  styleUrls: ['./buiness-edit-profile.component.css']
})
export class BuinessEditProfileComponent implements OnInit {
  
  dialogRef: MatDialogRef<DialogComponent>;
  profile: Profile;
  serviceType: string;
  globalProfileForm: FormGroup;
  earlyForm: any;
  filteredLocations: Observable<Location[]>;
  locations: Location[];

  latitude: number = -6.175387099999986;
  longitude: number = 106.82714748255614;
  radius: number = 5000;
  zoom: number;
  address: string;
  private geoCoder;
  @ViewChild('search', {static: false})
  public searchElementRef: ElementRef;
  deviceInfo = null;

  constructor(private profileService : ProfileService, private dialog: MatDialog, public locService: LocationService, private ngZone: NgZone, private mapsAPILoader: MapsAPILoader, private deviceService: DeviceDetectorService, private pageTitle: Title) {
    this.initializeGlobalForm();
   }

  ngOnInit() {
    this.pageTitle.setTitle('Edit Business Service');

    this.mapsAPILoader.load().then(() => {
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;
 
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["address"]
      });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
 
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          
          
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.getAddress(this.latitude, this.longitude);
          this.addCoordinates(this.longitude, this.latitude);
          this.zoom = 12;
        });
      });
    });

    this.profileService.getProfileDetails()
    .subscribe(profileDetails => {
      this.profile = profileDetails;

      if(profileDetails.host.grooming.geolocation.coordinates != null){
      this.longitude = +profileDetails.host.grooming.geolocation.coordinates[0];
      this.latitude = +profileDetails.host.grooming.geolocation.coordinates[1];
      this.mapsAPILoader.load().then(() => {
        this.getAddress(this.latitude, this.longitude);
      });
      this.globalProfileForm.patchValue(profileDetails.host.grooming)
      this.globalProfileForm.patchValue({
            service_type: 'grooming'
        });
      this.earlyForm = this.globalProfileForm.value;
      } else {
        this.globalProfileForm.patchValue({
          service_type: 'grooming',
          geolocation: {
            type: "Point",
            coordinates: ["",""]
            // radius: 1000
          },
          host_name:   profileDetails.host.grooming.host_name,
          is_company:  profileDetails.host.grooming.is_company,
             coverage_area:     {
                city:  profileDetails.host.grooming.coverage_area.city
             },
          tagline:     profileDetails.host.grooming.tagline
        });
        this.earlyForm = this.globalProfileForm.value;
      }

      this.locService.getCityList()
        .subscribe(location => {
              this.locations = location;
              this.filteredLocations = this.globalProfileForm.get('coverage_area').get('city').valueChanges
              .pipe(
                startWith(''),
                map(value => this.locService._filter(value, location))
              )
       });
    });

  }

  async saveGlobal(){
    if(this.globalProfileForm.value != this.earlyForm){
      this.profileService.becomeHostGrooming(this.globalProfileForm.value)
      .subscribe(editProfile => {
        this.globalProfileForm.patchValue({
          service_type: 'hotel'
        });
      });
      this.profileService.becomeHostHotel(this.globalProfileForm.value)
        .subscribe(editProfile => {
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: "Profile Updated"
            }
          });
        });
      // this.profileService.becomeHostGrooming(this.globalProfileForm.value)
      // .subscribe(editProfile => {
      //   this.dialogRef = this.dialog.open(DialogComponent, {
      //           data: {
      //             message: "Profile Updated"
      //           }
      //         });
      // })
     
    } else if(this.globalProfileForm.value == this.earlyForm){
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Profile Unchanged"
        }
      });
    }
  }

  initializeGlobalForm(){
    this.globalProfileForm =    new FormGroup({
      service_type:             new FormControl(''),
          host_name:            new FormControl('', [Validators.required]),
          is_company:           new FormControl(''),
             coverage_area:     new FormGroup({
                city:           new FormControl('')
             }),
          geolocation:          new FormGroup({
            type:               new FormControl(''),
            coordinates:        new FormArray([
              new FormControl(''),
              new FormControl('')
            ])
            // radius:             new FormControl('')
          }),
          tagline:              new FormControl('')
          // photo_urls:           new FormControl('')
          })
  }

  get globalCoordinates(){
    return this.globalProfileForm.get('geolocation').get('coordinates') as FormArray;
  }

  addCoordinates(longitude, latitude){
    this.globalCoordinates.patchValue([longitude, latitude]);
  }

  private setCurrentLocation(){
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        // this.latitude = position.coords.latitude;
        // this.longitude = position.coords.longitude;
        if(this.longitude != null && this.latitude != null){
          this.longitude = +this.longitude;
          this.latitude = +this.latitude;
          this.getAddress(this.latitude, this.longitude);
          this.addCoordinates(this.longitude, this.latitude);
          this.zoom = 15;
        } else {
          this.longitude = +106.82714748255614;
          this.latitude = +-6.175387099999986;
          this.getAddress(this.latitude, this.longitude);
          this.addCoordinates(this.longitude, this.latitude);
          this.zoom = 15;
        }
        
      });
    }
  }

  markerDragEnd(event: MouseEvent) {
    this.latitude = event.coords.lat;
    this.longitude = event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
    this.addCoordinates(this.longitude, this.latitude);
  }

  // getRadius(event: MouseEvent){
  //   this.globalProfileForm.get('geolocation').get('radius').patchValue(event);
  // }

  getAddress(latitude, longitude) {
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.zoom = 12;
          this.address = results[0].formatted_address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }

}
