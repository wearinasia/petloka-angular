import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Product } from '../../../../models/product';
import { ProductService } from '../../../../services/product.service';
import { ProfileService } from '../../../../services/profile.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../../components/shared/dialog/dialog.component';
import { DeviceDetectorService } from 'ngx-device-detector';
import { Title } from '@angular/platform-browser';
import { delay } from 'q';

@Component({
  selector: 'app-business-manage-account',
  templateUrl: './business-manage-account.component.html',
  styleUrls: ['./business-manage-account.component.css']
})
export class BusinessManageAccountComponent implements OnInit {

  private customerSession = JSON.parse(localStorage.getItem("customerSession"));
  customerId: string;
  idToken: string;
  schedule: boolean;
  hotelProducts: Product[];
  groomProducts: Product[];
  steps: number;

  dialogRef: MatDialogRef<DialogComponent>;

  constructor( private router: Router, private route: ActivatedRoute, private prodService: ProductService, private pageTitle: Title, private profileService: ProfileService, private dialog: MatDialog, public deviceService: DeviceDetectorService) {
   }

  ngOnInit(){
    this.pageTitle.setTitle('Manage Business Account');
    this.route.queryParams.subscribe(params => {
      this.steps = params.steps;
    });

      this.customerId = this.customerSession.localId;
      this.prodService.getProductList(this.customerId,"hotel")
      .subscribe(
          hotelProduct => {
          return this.hotelProducts = hotelProduct;
      });
      this.prodService.getProductList(this.customerId,"grooming")
        .subscribe(
          groomProduct => {
          return this.groomProducts = groomProduct;
      });
  }

  refreshData(){
    this.prodService.getProductList(this.customerId,"hotel")
    .subscribe(
        hotelProduct => {
        return this.hotelProducts = hotelProduct;
      })
    this.prodService.getProductList(this.customerId,"grooming")
      .subscribe(
        groomProduct => {
        return this.groomProducts = groomProduct;
      })
  }

  async grooming(){
      this.router.navigate(['account/business/manage-service'], {queryParams: {type: 'grooming', steps: 1}});
      await delay(1);
      this.route.queryParams.subscribe(async params => {
        if(params.steps != null){
          this.steps = params.steps;
          localStorage.setItem('step', this.steps.toString());
        }});
  }

  async hotel(){
    this.router.navigate(['account/business/manage-service'], {queryParams: {type: 'hotel', steps: 1}});
    await delay(1);
    this.route.queryParams.subscribe(async params => {
      if(params.steps != null){
        this.steps = params.steps;
        localStorage.setItem('step', this.steps.toString());
          }});
  }
}
