import { Component, OnInit, ViewChild } from '@angular/core';
import { Profile } from '../../../../../models/profile';
import { ProfileService } from '../../../../../services/profile.service';
import { AuthService } from '../../../../../services/auth.service';
import { FormControl, FormGroup, FormArray, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { CalendarComponent } from '@syncfusion/ej2-angular-calendars';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../../../components/shared/dialog/dialog.component';

@Component({
  selector: 'app-business-account-schedule',
  templateUrl: './business-account-schedule.component.html',
  styleUrls: ['./business-account-schedule.component.css']
})
export class BusinessAccountScheduleComponent implements OnInit {

  globalProfileForm: FormGroup;
  profile: Profile;
  type: string;
  steps: string;
  dialogRef: MatDialogRef<DialogComponent>;
  start;
  times = [
    '12:00 AM', '11:00 AM', '10:00 AM', '9:00 AM', '8:00 AM', '7:00 AM', '6:00 AM', 
    '5:00 AM', '4:00 AM', '3:00 AM', '2:00 AM', '1:00 AM' ,
    '12:00 PM', '11:00 PM', '10:00 PM', '9:00 PM', '8:00 PM', '7:00 PM', '6:00 PM', 
    '5:00 PM', '4:00 PM', '3:00 PM', '2:00 PM', '1:00 PM',
    '12:30 AM', '11:30 AM', '10:30 AM', '9:30 AM', '8:30 AM', '7:30 AM', '6:30 AM', 
    '5:30 AM', '4:30 AM', '3:30 AM', '2:30 AM', '1:30 AM',
    '12:30 PM', '11:30 PM', '10:30 PM', '9:30 PM', '8:30 PM', '7:30 PM', '6:30 PM', 
    '5:30 PM', '4:30 PM', '3:30 PM', '2:30 PM', '1:30 PM' 
  ]

  public minDate : Date = new Date();
  @ViewChild('calendar', {static: false})
  public calendarObj: CalendarComponent;

  constructor(private authService: AuthService, public profileService: ProfileService, private router: Router, private route: ActivatedRoute, private dialog: MatDialog) {
    this.initializeGlobalProfileForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.type = params.type;
    });
    this.profileService.getProfileDetails()
    .subscribe(editDetails => {
      // editDetails.host.grooming.operationalHours.weekDays.start = formatDate(editDetails.host.grooming.operationalHours.weekDays.start, 'h:mm a', 'id', '+0700');
      // editDetails.host.grooming.operationalHours.weekDays.end = formatDate(editDetails.host.grooming.operationalHours.weekDays.end, 'h:mm a', 'id', '+0700');
      this.globalProfileForm.patchValue({
        service_type: "grooming"
      });
      if(editDetails.host.grooming.operationalDays.weekDays == null){
        this.globalProfileForm.patchValue({
          service_type: 'grooming',
          operationalDays: {
            weekDays: ['']
          }
        })
      } else if(editDetails.host.grooming.operationalDays.weekDays.length == 0){
        editDetails.host.grooming.operationalDays.weekDays[0] = this.minDate.toISOString()
        this.globalProfileForm.patchValue(editDetails.host.grooming);
      }
      this.profile = editDetails;
    })
    
  }
  
  saveGlobalProfile(){
    if(this.type == "hotel"){
      this.globalProfileForm.patchValue({
        service_type: "hotel"
      })
      this.profileService.becomeHostHotel(this.globalProfileForm.value)
      .subscribe(editDetails => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: "Schedule Updated"
          }
        });
        this.profileService.setSchedule(false);
      });
    }
    if(this.type == "grooming"){
      this.globalProfileForm.patchValue({
        service_type: "grooming"
      })
    }
      this.profileService.becomeHostGrooming(this.globalProfileForm.value)
      .subscribe(editDetails => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: "Schedule Updated"
          }
        });
        this.profileService.setSchedule(false);
      });
  }

  onSelect(event){
      for (let i: number = 0; i < this.calendarObj.values.length; i++) {
        if(this.globalProfileDays.value.find(day => this.globalProfileDays.value)){
          this.removeDays(i);
        };
        this.setDays(i, this.calendarObj.values[i].toISOString());
      }
  }

  initializeGlobalProfileForm(){
    this.globalProfileForm =    new FormGroup({
      service_type:             new FormControl(''),
      operationalDays:          new FormGroup({
        weekDays:               new FormArray([])
      }),
      operationalHours:         new FormGroup({
        weekDays:               new FormGroup({
          end:                  new FormControl('', [Validators.required]),
          start:                new FormControl('', [Validators.required])
        })
      })
     })
  }

  get globalProfileDays(){
    return this.globalProfileForm.get('operationalDays').get('weekDays') as FormArray;
  }

  setDays(i, items){
    this.globalProfileDays.setControl(i,new FormControl(items));
  }

  removeDays(i){
    this.globalProfileDays.removeAt(i);
  }

}
