import { Component, OnInit, Input } from '@angular/core';
import { ProfileService } from '../../../../../services/profile.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Validators, FormArray, FormControl, FormGroup } from '@angular/forms';
import { ProductService } from '../../../../../services/product.service';
import { AuthService } from '../../../../../services/auth.service';
import { BusinessManageAccountComponent } from '../business-manage-account.component';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../../../components/shared/dialog/dialog.component';

@Component({
  selector: 'app-business-account-product',
  templateUrl: './business-account-product.component.html',
  styleUrls: ['./business-account-product.component.css']
})
export class BusinessAccountProductComponent implements OnInit{

  globalProductForm: FormGroup;
  type: string;
  steps: Number;
  pId: string;
  productDetails: any;
  dialogRef: MatDialogRef<DialogComponent>;
  @Input() serviceType: string;
  

  constructor(private authService: AuthService, public profileService: ProfileService, private router: Router, private route: ActivatedRoute, private prodService: ProductService, private business: BusinessManageAccountComponent, private dialog: MatDialog) { 
    this.initializeGlobalProductForm();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.type = params.type;
      if(this.type == "hotel"){
        this.serviceType = "Hotel";
        this.globalProductForm.reset();
        if(this.globalProductIncluding.length > 1){
          while(this.globalProductIncluding.length > 1){
            this.globalProductIncluding.removeAt(0);
          }
        }
      }
      else if(this.type == "grooming"){
        this.serviceType = "Grooming";
        this.globalProductForm.reset();
        if(this.globalProductIncluding.length > 1){
          while(this.globalProductIncluding.length > 1){
            this.globalProductIncluding.removeAt(0);
          }
        }
      }
    });

    this.route.queryParams.subscribe(params => {
      this.pId = params.pId;
      if(this.pId != null){
        this.prodService.getProductDetails(this.pId)
        .subscribe(productDetails => {
          this.globalProductForm.patchValue(productDetails);
          for(let i = 0;i < productDetails.including.length; i++){
            this.setProduct(i, productDetails.including[i]);
          }
        })
      }
    })    
    
  }

  checkSchedule(event){
    this.profileService.setSchedule(event.checked);
  }

  checkPhoto(event){
    this.profileService.setPhoto(event.checked);
  }

  saveGlobalProduct(){
    if(this.pId == null){

      if(this.serviceType == "Hotel"){
        this.profileService.createProductHotel(this.globalProductForm.value)
        .subscribe(async editProfile => {
          this.route.queryParams.subscribe(async params => {
            if(params.steps != null){
            this.steps = params.steps;
            localStorage.setItem('step', this.steps.toString());
          }});
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: "Product Updated"
            }
          });
        this.globalProductForm.reset();
        this.business.refreshData();
        })
      }
      else if(this.serviceType == "Grooming"){
        this.profileService.createProductGroom(this.globalProductForm.value)
        .subscribe(async editProfile => {
          this.route.queryParams.subscribe(async params => {
            if(params.steps != null){
              this.steps = params.steps;
              localStorage.setItem('step', this.steps.toString());
            }});
          this.dialogRef = this.dialog.open(DialogComponent, {
            data: {
              message: "Product Updated"
            }
          });
        this.globalProductForm.reset();
        this.business.refreshData();
        })
      }

    } else if (this.pId != null){
      this.prodService.editProductBypId(this.globalProductForm.value, this.pId)
      .subscribe(editProfile => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: "Product Edited"
          }
        });
        this.business.refreshData();
      })
    }
  }

  initializeGlobalProductForm(){
    this.globalProductForm =       new FormGroup({
      about:          new FormControl(''),
      price:          new FormControl(null, [Validators.pattern(/^-?(0|[0-9]\d*)?$/)]),
      product_name:   new FormControl('', [Validators.required]),
      including:      new FormArray([
        new FormControl('')
      ])
    })
  }

  get globalProductIncluding(){
    return this.globalProductForm.get('including') as FormArray;
  }

  addGlobalProductIncluding(){
    this.globalProductIncluding.push(new FormControl(''));
  }

  setProduct(i, items){
    this.globalProductIncluding.setControl(i,new FormControl(items));
  }

  deleteIncluding(index: number){
    this.globalProductIncluding.removeAt(index);
  }

}
