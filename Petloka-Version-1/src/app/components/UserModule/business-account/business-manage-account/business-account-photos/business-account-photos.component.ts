import { Component, OnInit } from '@angular/core';
import { Profile } from '../../../../../models/profile';
import { ProfileService } from '../../../../../services/profile.service';
import { AuthService } from '../../../../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularFireStorage, AngularFireUploadTask, AngularFireStorageReference } from 'angularfire2/storage';
import { Observable } from 'rxjs';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../../../../components/shared/dialog/dialog.component';
import { Location } from '@angular/common';

@Component({
  selector: 'app-business-account-photos',
  templateUrl: './business-account-photos.component.html',
  styleUrls: ['./business-account-photos.component.css']
})
export class BusinessAccountPhotosComponent implements OnInit {

  dialogRef: MatDialogRef<DialogComponent>;
  profile: Profile;
  ServicePhoto: any[]=[];
  ref: AngularFireStorageReference;
  task: AngularFireUploadTask;
  uploadProgress: Observable<number>;
  downloadURL = [];
  randomId: string;
  type: string;
  steps: string;

  constructor(private authService: AuthService, private dialog: MatDialog, private profileService: ProfileService, private router: Router, private route: ActivatedRoute, private afStorage: AngularFireStorage, private location: Location) { 
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.type = params.type;
    })
    const stepp = JSON.parse(localStorage.getItem('step'));
    this.route.queryParams.subscribe(async params => {
      if(params.steps != null){
        this.steps = params.steps;
      }
    })
    if(this.steps != stepp){
      this.router.navigate(['account/business/manage-service']);
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Fill previous form First"
        }
      });
    }
  }

  async upload(event){
    for (let i = 0; i < event.target.files.length; i++) {
      this.randomId = Math.random().toString(36).substring(2);
      this.ref = this.afStorage.ref(this.randomId);
      this.task = this.afStorage.upload(this.randomId, event.target.files[i]);
      this.task.then((response) => {
        response.ref.getDownloadURL().then((url) => {
          this.downloadURL[i] = url;
        })
      });
      this.uploadProgress = this.task.percentageChanges();
    }
  }

submit(url: any[]){
  if(this.type == 'hotel'){
    this.profileService.becomeHostHotel({photo_urls: url})
    .subscribe(response => {
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Photo Uploaded"
        }
      });
      localStorage.removeItem('step');
      this.router.navigateByUrl('/account/business/manage-service')
    })
  } else if(this.type == 'grooming'){
    this.profileService.becomeHostGrooming({photo_urls: url})
    .subscribe(response => {
      this.dialogRef = this.dialog.open(DialogComponent, {
        data: {
          message: "Photo Uploaded"
        }
      });
      localStorage.removeItem('step');
      this.router.navigateByUrl('/account/business/manage-service')
    })
  }
  }

}
