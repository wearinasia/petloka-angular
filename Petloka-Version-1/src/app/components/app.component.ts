import { Component } from '@angular/core';
import { ConnectionService } from 'ng-connection-service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../components/shared/dialog/dialog.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  status = 'ONLINE';
  isConnected = true;

  dialogRef: MatDialogRef<DialogComponent>;
  constructor(private connectionService: ConnectionService, private dialog: MatDialog) {
    this.connectionService.monitor().subscribe(isConnected => {
      this.isConnected = isConnected;
      if (this.isConnected) {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: 'Status : Online'
          }
        });
      }
      else {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: 'Check your internet connection'
          }
        });
      }
    })
  }
}
