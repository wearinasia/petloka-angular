import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../../services/product.service';
import { Product } from '../../../models/product';
import { Router, ActivatedRoute } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Profile } from 'src/app/models/profile';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products : Product[];
  user : Profile;
  message: string;
  
  constructor(private prodService: ProductService, private route: ActivatedRoute, private pageTitle: Title) { }

  ngOnInit() {

      this.getUserDetails();
      this.getProductDetails();
      this.pageTitle.setTitle('Product Order');
     
  }

  
  getProductDetails(){
    const uId  = this.route.snapshot.paramMap.get('uId');
    const Type  = this.route.snapshot.paramMap.get('type');
    this.prodService.getProductList(uId,Type)
    .subscribe(
      product => {
        this.products = product;
      })
  }

  getUserDetails(){
    const uId  = this.route.snapshot.paramMap.get('uId');
    this.prodService.getUserDetails(uId)
    .subscribe(
      user => {
        this.user = user;
        this.message = "done";
      })
  }

}
