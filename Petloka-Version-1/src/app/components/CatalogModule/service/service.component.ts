import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ServiceService } from '../../../services/service.service';
import { Service } from '../../../models/service';
import { delay, startWith, map } from 'rxjs/operators';
import { Title } from '@angular/platform-browser';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { faChevronRight } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  id_token: string;
  private type: string;
  private location: string;
  message: string;
  title: string;
  description: string;

  //Font Awesome
  faChevronRight = faChevronRight;

  searchServiceByName = new FormControl('');
  filteredServices: Observable<Service[]>;
  services: Service[];

  constructor(private router: Router, private servService: ServiceService, private route: ActivatedRoute, public pageTitle: Title) { }

  async ngOnInit() {
    await delay(2);
    this.type = this.route.snapshot.paramMap.get('type');
    this.setPageTitleByType(this.type);

    this.route.queryParams.subscribe(params => {
      this.location = params.location;
    })
      if(this.location == null){
        this.servService.getServiceTypeView(this.type)
          .subscribe(service => {
          this.services = service;
              this.filteredServices = this.searchServiceByName.valueChanges
                .pipe(
                startWith(''),
                map(value => this.filter(value, service))
              )
          this.message = "done";
        });
      } else {
        this.route.queryParams.subscribe(params => {
          this.location = params.location;
          this.servService.getServiceTypeViewByLocation(this.type, this.location)
          .subscribe(service => {
            this.services = service;
            this.filteredServices = this.searchServiceByName.valueChanges
                .pipe(
                startWith(''),
                map(value => this.filter(value, service))
              )
            this.message = "done";
          })
        })
      }
  }

  private filter(value: string, service): Service[]{
    const filterValue = value.toLowerCase();

    return service.filter(service => service.host_name.toLowerCase().includes(filterValue));
  }

  getHotel(){
    this.route.queryParams.subscribe(params => {
      this.location = params.location;
    });
    if(this.location == null){
      this.servService.getServiceTypeView('hotel')
        .subscribe(async service => {
        this.router.navigateByUrl('service/type/hotel');
        this.setPageTitleByType('hotel');
        this.services = service;
        this.filteredServices = this.searchServiceByName.valueChanges
                .pipe(
                startWith(''),
                map(value => this.filter(value, service))
              )
        this.message = "done";
      });
    } else {
      this.route.queryParams.subscribe(params => {
        this.location = params.location;
        this.servService.getServiceTypeViewByLocation('hotel', this.location)
        .subscribe(service => {
          this.router.navigate(['service/type/hotel'], {queryParams: {location: this.location}});
          this.setPageTitleByType('hotel');
          this.services = service;
          this.filteredServices = this.searchServiceByName.valueChanges
                .pipe(
                startWith(''),
                map(value => this.filter(value, service))
              )
          this.message = "done";
        })
      })
    }
  }

  getGrooming(){
    this.route.queryParams.subscribe(params => {
      this.location = params.location;
    });
    if(this.location == null){
      this.servService.getServiceTypeView('grooming')
        .subscribe(async service => {
        this.router.navigateByUrl('service/type/grooming');
        this.setPageTitleByType('grooming');
        this.services = service;
        this.filteredServices = this.searchServiceByName.valueChanges
                .pipe(
                startWith(''),
                map(value => this.filter(value, service))
              )
        this.message = "done";
      });
    } else {
      this.route.queryParams.subscribe(params => {
        this.location = params.location;
        this.servService.getServiceTypeViewByLocation('grooming', this.location)
        .subscribe(service => {
          this.router.navigate(['service/type/grooming'], {queryParams: {location: this.location}});
          this.setPageTitleByType('grooming');
          this.services = service;
          this.filteredServices = this.searchServiceByName.valueChanges
                .pipe(
                startWith(''),
                map(value => this.filter(value, service))
              )
          this.message = "done";
        });
      })
    }
  }

  setPageTitleByType(type: string){
    if(type == 'hotel'){
      this.pageTitle.setTitle('Hotel');
      this.description = "Hotel Murah Mantap";
    } else if (type == 'grooming'){
      this.pageTitle.setTitle('Grooming');
      this.description = "Grooming Cepat Bersih Mantap";
    }
  }
}
