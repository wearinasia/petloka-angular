import { Component, OnInit } from '@angular/core';
import { Auth } from 'src/app/models/auth';
import { AuthService } from '../../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {

  id_token: string;
  customer: Auth;

  constructor(private authService: AuthService,private router: Router) { }

  ngOnInit() {
    this.id_token = localStorage.getItem('id_token');
  }

   forgot(password: string): void{
     this.authService.forgot(this.id_token, password)
     .subscribe(customer => {
       localStorage.removeItem('id_token');
       this.router.navigateByUrl('/home');
       return this.customer = customer;
     })
   }

}
