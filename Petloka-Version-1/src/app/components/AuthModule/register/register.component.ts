import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Auth } from '../../../models/auth';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Title } from '@angular/platform-browser';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../../shared/dialog/dialog.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  dialogRef : MatDialogRef<DialogComponent>;
  customer : Auth;
  registerForm: FormGroup;
  password = new FormControl('');
  email = new FormControl('');
  hide = true;

  constructor(private authService: AuthService, private pageTitle: Title, private dialog: MatDialog, private router: Router) {
    this.initializeForm();
  }

  ngOnInit() {
    this.pageTitle.setTitle('Register');
  }

  register(){
    this.authService.register(this.registerForm.value)
    .subscribe(customer => {
      this.authService.setRegisterSession(customer);
      this.authService.registerapp().subscribe(
        response => {
        this.dialogRef = this.dialog.open(DialogComponent, {
          data: {
            message: response.message
          }
        })
        this.authService.login(this.registerForm.value)
        .subscribe(login => {
          this.authService.setCustomerSession(login);
          this.router.navigateByUrl('account/customer/booking');
        })
      });
      return this.customer = customer;
    })
  }

  initializeForm(){
    this.registerForm =  new FormGroup({
      email             :         new FormControl('', [Validators.required]),
      password          :         new FormControl ('', [Validators.required]),
      returnSecureToken :         new FormControl(true)
    });
  }

}
