import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
import { ProfileService } from '../../services/profile.service';
import { AuthService } from '../../services/auth.service';
import { map } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class AuthGuard implements CanActivate {
  constructor(private router: Router, private route: ActivatedRoute, private authService: AuthService, private profileService: ProfileService) { }

  step: number;
  appidToken: string;

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      
      if(this.authService.getLoggedIn() != false){
        return this.profileService.getProfileDetails().pipe(
        map((auth) =>{
            if(auth){
              return true;
            }
              this.router.navigateByUrl('/login');
              return false;
        },
        (error: HttpErrorResponse) => {
            this.router.navigateByUrl('/login');
            return false;
        })
      )}
      else {
        this.router.navigateByUrl('/login');
        return false;
      }
  
  }
}