import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { Auth } from '../../../models/auth';
import { Router,ActivatedRoute } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  //authService: any;

  loginForm: FormGroup;
  auth: Auth;
  isLoggedIn: boolean;
  token: string;
  id_token: string;
  step: Number;
  lastUrl: string;

  hide = true;

  constructor(private authService: AuthService, private router: Router, private route: ActivatedRoute, private pageTitle: Title) {
    this.initializeForm();
  }

  ngOnInit() {
    this.pageTitle.setTitle('Login');
  }

   login(){
    this.route.queryParams.subscribe(params => {
      this.step = Number(params.step);
    });
    
     this.authService.login(this.loginForm.value)
     .subscribe(auth => {
        this.authService.setCustomerSession(auth);
        this.route.queryParams.subscribe(params => {
          this.lastUrl = params.lastUrl;
        })
        if(this.lastUrl == null){
          this.router.navigateByUrl('account/customer/booking');
        }
        else if(this.lastUrl == 'createbooking') {
          this.router.navigateByUrl('createbooking');
        }
     });
   }

  initializeForm(){
    this.loginForm =  new FormGroup({
      email             :         new FormControl('', [Validators.required]),
      password          :         new FormControl ('', [Validators.required]),
      returnSecureToken :         new FormControl(true)
    });
  }
}
