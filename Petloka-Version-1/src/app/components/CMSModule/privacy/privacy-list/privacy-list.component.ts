import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-privacy-list',
  templateUrl: './privacy-list.component.html',
  styleUrls: ['./privacy-list.component.css']
})
export class PrivacyListComponent implements OnInit {

  no: number;
  privacy;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.no = +this.route.snapshot.paramMap.get('No');
    if(this.no == 1){
      this.privacy = 'PERSONAL INFORMATION WHICH WE COLLECT';
    }else if(this.no == 2){
      this.privacy = 'THE USE OF PERSONAL INFORMATION WHICH WE COLLECT';
    }else if(this.no == 3){
      this.privacy = 'SHARING OF PERSONAL INFORMATION WHICH WE COLLECT';
    }else if(this.no == 4){
      this.privacy = 'RETENTION OF PERSONAL INFORMATION';
    }else if(this.no == 5){
      this.privacy = 'ACCESS AND CORRECTION OF PERSONAL INFORMATION';
    }else if(this.no == 6){
      this.privacy = 'WHERE WE STORE YOUR PERSONAL INFORMATION';
    }else if(this.no == 7){
      this.privacy = 'SECURITY OF YOUR PERSONAL INFORMATION';
    }else if(this.no == 8){
      this.privacy = 'CHANGES TO THIS PRIVACY POLICY';
    }else if(this.no == 9){
      this.privacy = 'ACKNOWLEDGMENT AND CONSENT';
    }else if(this.no == 10){
      this.privacy = 'MARKETING AND PROMOTIONAL MATERIAL';
    }else if(this.no == 11){
      this.privacy = 'ANONYMOUS DATA';
    }else if(this.no == 12){
      this.privacy = 'THIRD PARTY PLATFORMS';
    }else if(this.no == 13){
      this.privacy = 'HOW TO CONTACT US';
    }
  }

}
