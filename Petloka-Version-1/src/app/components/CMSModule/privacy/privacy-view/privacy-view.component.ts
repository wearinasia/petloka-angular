import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-privacy-view',
  templateUrl: './privacy-view.component.html',
  styleUrls: ['./privacy-view.component.css']
})
export class PrivacyViewComponent implements OnInit {

  constructor(private pageTitle: Title, private router: Router) { }

  ngOnInit() {
    this.pageTitle.setTitle('Privacy');
  }

  viewPrivacy(no: number){
    this.router.navigateByUrl(`/privacy/${no}`);
  }

}
