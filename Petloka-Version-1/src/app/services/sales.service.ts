import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { OrderList } from '../models/orderlist'

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private getAllOrderUrl = '/api/v1/bookings';

  constructor(private http: HttpClient) { }

  getAllOrder(): Observable<OrderList[]>{
    return this.http.get<OrderList[]>(this.baseUrl + this.getAllOrderUrl);
  }

  changeStatus(bId: string,status): Observable<any>{
    const changeStatusOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization' : `Bearer web_test_id`
      })
    }
    return this.http.put<any>(this.baseUrl + this.getAllOrderUrl + `/${bId}/status`, status, changeStatusOptions);
  }

  async updateStatus(bookingId: string, status: string): Promise<any> {
    const url = 'https://spry-blade-228107.appspot.com/api/v1/payment/handling';
    const transactionStatus = (status === 'PAYMENT_SUCCESS') ? 'capture' : 'manual';
    const body = {
      transaction_time: new Date().toLocaleString(),
      transaction_status: transactionStatus,
      status_message: 'manual payment notification',
      status_code: '200',
      settlement_time: new Date().toLocaleString(),
      payment_type: 'manual',
      order_id: bookingId,
    };
    return await this.http.post(url, body, {headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })}).toPromise();
  }
}
