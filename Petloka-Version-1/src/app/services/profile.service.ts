import { Injectable } from '@angular/core';
import { Profile } from '../models/profile';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { Auth } from '../models/auth';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {

  private profileOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private profileUrl = '/api/v1/user/details';
  private becomeHostHotelUrl = '/api/v1/hosts/hotel';
  private becomeHostGroomingUrl = '/api/v1/hosts/grooming';
  private productGroomUrl = '/api/v1/myproducts/grooming';
  private productHotelUrl = '/api/v1/myproducts/hotel';
  private profileNoLoginUrl = '/api/v1/hosts/';
  scheduleChecked: boolean;
  photoChecked: boolean;

  constructor(private http: HttpClient, private authService: AuthService) {}

  setPhoto(photo: boolean){
    this.photoChecked = photo;
  }

  getPhoto(){
    return this.photoChecked;
  }

  setSchedule(schedule: boolean){
    this.scheduleChecked = schedule;
  }

  getSchedule(){
    return this.scheduleChecked;
  }

  setprofileOptions(){
    this.profileOptions.headers = this.profileOptions.headers.set('Authorization', `Bearer ${this.authService.getCustomerToken()}`)
  }

  editProfileNoLogin(uId: string, type: string, details){
    return this.http.post<Profile>(this.baseUrl + this.profileNoLoginUrl + uId + '/' + type, details);
  }

  getProfileDetails() : Observable<Profile>{
    this.setprofileOptions();
    return this.http.get<Profile>(this.baseUrl + this.profileUrl, this.profileOptions);
  }

  editProfile(profileDetails) : Observable<any>{
    this.setprofileOptions();
    return this.http.put<any>(this.baseUrl + this.profileUrl, profileDetails , this.profileOptions);
  }

  becomeHostHotel(profileDetails) : Observable<any>{
    this.setprofileOptions();
    return this.http.post<any>(this.baseUrl + this.becomeHostHotelUrl, profileDetails, this.profileOptions);
  }

  becomeHostGrooming(profileDetails) : Observable<any>{
    this.setprofileOptions();
    return this.http.post<any>(this.baseUrl + this.becomeHostGroomingUrl, profileDetails, this.profileOptions);
  }

  createProductHotel(productDetails) : Observable<any>{
    this.setprofileOptions();
    return this.http.post<any>(this.baseUrl + this.productHotelUrl, productDetails, this.profileOptions);
  }

  createProductGroom(productDetails) : Observable<any>{
    this.setprofileOptions();
    return this.http.post<any>(this.baseUrl + this.productGroomUrl, productDetails, this.profileOptions);
  }
}
