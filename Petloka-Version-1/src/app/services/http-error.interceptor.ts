import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError, map } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogComponent } from '../components/shared/dialog/dialog.component';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

export class HttpErrorInterceptor implements HttpInterceptor {

  dialogRef: MatDialogRef<DialogComponent>

  constructor(public dialog: MatDialog, private router: Router, private authService: AuthService) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request)
          .pipe(
            retry(1),
            catchError((error: HttpErrorResponse) => {
              let errorMessage = '';
              if (error.error instanceof ErrorEvent) {
                errorMessage = `Error: ${error.error.message}`;
              } else {
                if(error.error.message == 'unable to verify the token provided'){
                  errorMessage = "Session Expired";
                  this.authService.logout();
                  this.router.navigateByUrl('/login');
                }
                 else if(error.error.msg == 'user not found'){
                   errorMessage = "Not a valid ID";
                   this.router.navigateByUrl('');
                 }
                 else {
                  errorMessage = `Message: ${error.error.error.message}`;
                }
              }
              this.dialogRef = this.dialog.open(DialogComponent, {
                data: {
                  message: errorMessage
                }
              });
              return throwError(errorMessage);
            })
          )
      }
 }