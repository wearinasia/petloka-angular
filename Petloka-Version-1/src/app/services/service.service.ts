import { Injectable } from '@angular/core';
import { Service } from '../models/service';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  services: Service[];
  serviceType: string;

  private serviceViewUrl = '/api/v1/hosts/';
  private locationUrl = '?location=';
  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private currentLocationUrl = '/api/v1/hosts/';

  constructor(private http: HttpClient) {}

  getServiceTypeView(type: string): Observable<Service[]>{
    return this.http.get<Service[]>(this.baseUrl + this.serviceViewUrl + type);
  }

  getServiceTypeViewByLocation(type: string, location: string): Observable<Service[]>{
    return this.http.get<Service[]>(this.baseUrl + this.serviceViewUrl + type + this.locationUrl + location);
  }

  setServiceType(serviceType: string){
    this.serviceType = serviceType;
  }

  getServiceTypeViewByCurrentLocation(type: string, latitude: number, longitude: number): Observable<Service>{
    return this.http.get<Service>(this.baseUrl + this.currentLocationUrl + type + '?coordinate=' + `${latitude},${longitude}`);
  }

  getServiceType(){
    return this.serviceType;
  }


}
