import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Product } from '../models/product';
import { Observable } from 'rxjs';
import { Profile } from '../models/profile';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  products: Product[];
  private productOption = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : `Bearer web_test_id`
    })
  }
  
  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private productListUrl = '/api/v1/products?';
  private userURL    = '/api/v1/user/';
  private productUrl = '/api/v1/products/';

  
  constructor(private http: HttpClient) {}

  getProductList(uId : string,type : string): Observable<Product[]>{
    return this.http.get<Product[]>(this.baseUrl+this.productListUrl+'host='+uId+'&type='+type);
  }

  getUserDetails(uId : string): Observable<Profile>{
    return this.http.get<Profile>(this.baseUrl+this.userURL+uId+'/details', this.productOption);
  }

  getProductDetails(pId: string): Observable<Product>{
    return this.http.get<Product>(this.baseUrl + this.productUrl + pId);
  }

  deleteProductBypId(pId: string): Observable<any>{
    return this.http.delete<any>(this.baseUrl + this.productUrl + pId, this.productOption);
  }

  editProductBypId(productDetails, pId: string): Observable<any>{
    return this.http.put<any>(this.baseUrl + this.productUrl + pId, productDetails, this.productOption);
  }
}