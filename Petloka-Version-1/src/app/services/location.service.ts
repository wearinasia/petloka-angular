import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Location } from '../models/location';

@Injectable({
  providedIn: 'root'
})
export class LocationService {

  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private cityListUrl = '/api/v2/list/cities';
  private matcher: string;

  constructor(private http: HttpClient) { }

  getCityList(): Observable<Location[]>{
    return this.http.get<Location[]>(this.baseUrl + this.cityListUrl);
  }

  check(e, locations){
    if(locations.find(loc => loc.name === e)){
      this.matcher = "Ok";
    } else {
      this.matcher = "Error";
    }
  }

  _filter(value: string, location): Location[]{
    const filterValue = value.toLowerCase();

    return location.filter(location => location.name.toLowerCase().includes(filterValue));
  }

  getMatcher(){
    return this.matcher;
  }
}
