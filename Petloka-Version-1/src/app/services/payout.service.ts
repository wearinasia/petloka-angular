import { Injectable } from '@angular/core';
import { PayoutList } from '../models/payoutlist';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PayoutService {

  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private payoutListUrl = '/api/v1/payouts';
  private payoutListbyIdUrl = '/api/v1/payouts/';
  private payoutOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization' : `Bearer web_test_id`
    })
  }

  constructor(private http: HttpClient) { 
    
  }

  getPayoutList(): Observable<PayoutList[]>{
    return this.http.get<PayoutList[]>(this.baseUrl + this.payoutListUrl, this.payoutOptions)
  }

  getPayoutbyId(bId: string): Observable<PayoutList>{
    return this.http.get<PayoutList>(this.baseUrl + this.payoutListbyIdUrl + bId, this.payoutOptions);
  }

  changePayoutStatus(bId: string, status): Observable<any>{
    return this.http.post<any>(this.baseUrl + this.payoutListbyIdUrl + bId, status, this.payoutOptions);
  }
}
