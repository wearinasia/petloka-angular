import { Injectable, OnInit } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Auth } from '../models/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {

  private loginUrl = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc';
  private registerUrl = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc';
  private forgotUrl = 'https://www.googleapis.com/identitytoolkit/v3/relyingparty/setAccountInfo?key=AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc';
  private registerAppUrl = '/api/v1/user/authenticate';
  private baseUrl = 'https://spry-blade-228107.appspot.com';

  idToken: string;
  isLoggedIn: boolean;
  auth: Auth;

  private registerOptions = { 
    headers: new HttpHeaders({
    'Content-Type' : 'application/json'
  })
  }
  
  setregisterOptions(idToken){
    this.registerOptions.headers = this.registerOptions.headers.set('Authorization', `Bearer ${idToken}`);
  }

  ngOnInit(): void {
  }

  constructor(private http: HttpClient) {
  }

   login(loginDetails): Observable<Auth> {
     return this.http.post<Auth>(this.loginUrl, loginDetails);
   }

   register(registerDetails): Observable<Auth>{
      return this.http.post<Auth>(this.registerUrl, registerDetails);
   }

    registerapp(): Observable<any> {
       return this.http.get<any>(this.baseUrl+this.registerAppUrl, this.registerOptions);
    }

   forgot(idtoken: string, password: string): Observable<Auth>{
     return this.http.post<Auth>(this.forgotUrl,{
       idToken: idtoken,
       password: password,
       returnSecureToken: "true"
     })
   }

   logout(){
     localStorage.removeItem('customerSession');
   }

   setRegisterSession(authResult){
     this.setregisterOptions(authResult.idToken);
   }
   
   setCustomerSession(authResult){
    localStorage.setItem('customerSession', JSON.stringify(authResult));
   }

   getCustomerToken(){
     this.auth = JSON.parse(localStorage.getItem('customerSession'));
     return this.auth.idToken;
   }

   getCustomerId(){
     this.auth = JSON.parse(localStorage.getItem('customerSession'));
     if(this.auth != null){
      return this.auth.localId;
     } else {
       return null;
     }
   }

   setLoggedIn(loggedIn){
    this.auth = JSON.parse(localStorage.getItem('customerSession'));
    this.isLoggedIn = loggedIn;
   }

   getLoggedIn(): boolean{
    this.auth = JSON.parse(localStorage.getItem('customerSession'));
    if(this.auth != null){
     return true;
    } else {
      return false;
    }
   }
}
