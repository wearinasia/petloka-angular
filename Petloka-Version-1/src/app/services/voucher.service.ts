import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class VoucherService {

  private voucherOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    params: new HttpParams({})
  }

  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private voucherUrl = '/api/v2/voucher/get';
  private validateVoucherUrl = '/api/v2/voucher/';
  private voucherRulesUrl = '/api/v2/promo';

  setvoucherOptions(){
    this.voucherOptions.headers = this.voucherOptions.headers.set('Authorization', `Bearer ${this.authService.getCustomerToken()}`);
  }

  constructor(private authService: AuthService, private http: HttpClient) {}

  getVoucherAmount(price: string, voucherCode: string){
    this.setvoucherOptions();
    this.voucherOptions.params = this.voucherOptions.params.set("price", price).set("voucherCode", voucherCode);
    return this.http.get<any>(this.baseUrl + this.voucherUrl, this.voucherOptions);
  }

  getVoucherRulesList(){
    return this.http.get<any>(this.baseUrl + this.voucherRulesUrl);
  }

  voucherValidation(voucherCode: string, pId: string){
    this.setvoucherOptions();
    this.voucherOptions.params = this.voucherOptions.params.set('product_id', pId);
    return this.http.get<any>(this.baseUrl + this.validateVoucherUrl + voucherCode, this.voucherOptions);
  }
}
