import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { MyBooking } from '../models/mybooking';
import { BookingDetails } from '../models/bookingdetails';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BookingService{
  
  private bookingOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  private bookingOptionsV1 = {
    headers: new HttpHeaders({
      'Content-Type' : 'application/json',
      'Accept-Version' : '1.1'
    })
  }

  private bookingPaymentOptions = { 
    headers: new HttpHeaders({
    'Content-Type' : 'application/json',
    'Accept' : 'application/json',
    'Authorization' : 'Basic U0ItTWlkLXNlcnZlci01cUpPZUhvX0hXSXFLdHRIWG5UUzQ2R3Q6'
  })
  }

  private baseUrl = 'https://spry-blade-228107.appspot.com';
  private bookingUrl = '/api/v1/products/';

  private myBookingUrl = '/api/v1/mybookings';
  private myOrderUrl = '/api/v1/myproducts/bookings';
  private myBookingDetailsUrl = '/api/v1/bookings/';
  private getTransportationFeeUrl = '/api/v2/service/calculate';

  private midTransBaseUrl = 'https://api.sandbox.midtrans.com';
  private midTransPaymentUrl = '/v2/charge';

  constructor(private http: HttpClient, private authService: AuthService) {
  }

  setbookingOptions(){
    this.bookingOptions.headers = this.bookingOptions.headers.set('Authorization', `Bearer ${this.authService.getCustomerToken()}`);
  }

  setbookingOptionsV1(){
    this.bookingOptionsV1.headers = this.bookingOptionsV1.headers.set('Authorization', `Bearer web_test_id`);
  }

  createBooking(details, pId: string): Observable<any>{
      this.setbookingOptions();
      return this.http.post(this.baseUrl + this.bookingUrl + `${pId}/book`, details , this.bookingOptions);
   }
  
   getMyBooking(): Observable<MyBooking[]>{
     this.setbookingOptions();
     return this.http.get<MyBooking[]>(this.baseUrl + this.myBookingUrl, this.bookingOptions)
   }

   getMyOrder(): Observable<MyBooking[]>{
    this.setbookingOptions();
    return this.http.get<MyBooking[]>(this.baseUrl + this.myOrderUrl, this.bookingOptions)
  }

   myBookingDetails(bId: string): Observable<BookingDetails>{
     this.setbookingOptionsV1();
     return this.http.get<BookingDetails>(this.baseUrl + this.myBookingDetailsUrl + bId, this.bookingOptionsV1)
   }

   bookingPayment(details): Observable<any>{
     return this.http.post<any>(this.midTransBaseUrl + this.midTransPaymentUrl, details, this.bookingPaymentOptions)
   }

   getTransportationFee(origin, destination): Observable<any>{
     const params = new HttpParams().set("origin_lon",origin.longitude).set("origin_lat",origin.latitude).set("dest_lon",destination.longitude).set("dest_lat",destination.latitude);
     return this.http.get<any>(this.baseUrl + this.getTransportationFeeUrl,{params: params});
   }
}
