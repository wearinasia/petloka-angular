import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'httprequest';

  constructor(private http: HttpClient){}

  ngOnInit(): void{
    this.http.get('https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=AIzaSyD5ybGXI0a1iFdJc8AwRWgS0fVBcReV-Pc').subscribe(data => {
      console.log(data);
    });
  }
}
