import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
 
import { User } from '../models/user';
import { Injectable } from '@angular/core';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
    providedIn: 'root'
})

export class UserService {
    private userUrl = 'api/user';

    constructor(
        private http: HttpClient){}

    getUsers() : Observable<User[]>{
        return this.http.get<User[]>(this.userUrl);
    }

    addUser(user: User): Observable<User> {
        return this.http.post<User>(this.userUrl, user, httpOptions);
    }
}