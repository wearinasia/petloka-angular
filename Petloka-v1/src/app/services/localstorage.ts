import { InMemoryDbService } from 'angular-in-memory-web-api';
import { User } from '../models/user';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService{
    createDb(){
        const user = [
            { id: 1, name: 'Alvin', phone: '0812121212', bio:'Testtsstss'}
        ];
        return {user};
    }
    genId(user: User[]): number {
        return user.length > 0 ? Math.max(...user.map(user => user.id)) + 1 : 1;
      }
}