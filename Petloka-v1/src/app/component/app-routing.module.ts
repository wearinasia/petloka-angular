import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { NavbarComponent } from '../shared/navbar/navbar.component';
import { ServiceComponent } from '../component/service/service.component';
import { ProductComponent } from '../component/product/product.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'dashboard'},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'navbar', component: NavbarComponent},
  { path: 'service', component: ServiceComponent},
  { path: 'product', component: ProductComponent},

  { path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
