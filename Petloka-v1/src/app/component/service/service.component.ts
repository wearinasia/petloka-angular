import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../../models/user';
import { UserService } from '../../services/user.service';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-service',
  templateUrl: './service.component.html',
  styleUrls: ['./service.component.css']
})
export class ServiceComponent implements OnInit {

  forms: any={};
  users: User[];
  name = new FormControl('');
  phone = new FormControl('');
  bio = new FormControl('');

  constructor(private userService: UserService) {
    if(localStorage.getItem("name") == null){
      this.forms.names=''
    }else{
       this.forms=JSON.parse(localStorage.getItem("name"));
    }
  }

  ngOnInit() {
    this.getUser();
  }

  saveData(){
    localStorage.setItem("name",JSON.stringify(this.forms));
  }

  deleteData(){
    localStorage.clear();
  }

  getUser(): void{
    this.userService.getUsers()
    .subscribe(users => this.users = users);
  }

  add(name: string, phone: string,bio: string,bname: string): void{
    name=name.trim();
    phone=phone.trim();
    bio=bio.trim();
    bname=bname.trim();
    if(!name) { return; }
    this.userService.addUser({ name,phone,bio,bname } as User)
      .subscribe(user => {
        this.users.push(user);
      });
  }

}
